#include <iostream>
#include <fstream>

#include <opencv2/opencv.hpp>

#include "IMUFileReader.h"

using namespace std;

string dataNum = "05";
bool drawGraph(vector<cv::Point3f> data, int type, string name, float scaleData = 1.0f);

void main() {
	char temp[50];
	printf("data num : ");
	scanf("%s", temp);
	dataNum = string(temp);

	IMUFileReader androidFileReader("../../../AndroidData/" + dataNum, FileType::AndroidFile);
	IMUFileReader iosFileReader("../../../iOSData/" + dataNum, FileType::iOSFile);

	int count = 0;

	long long int iosAccMillis = -1;
	long long int iosGyroMillis = -1;
	long long int iosImageMillis = -1;

	long long int andAccMillis = -1;
	long long int andGyroMillis = -1;
	long long int andImageMillis = -1;

	float r[3];
	float a[3];

	bool eof = true;

	vector<cv::Point3f> iosAcc;
	vector<cv::Point3f> iosGyro;
	vector<cv::Point3f> andAcc;
	vector<cv::Point3f> andGyro;

	while (eof) {
		bool isiOSSuccess = true;
		bool isAndSuccess = true;

		isiOSSuccess = iosFileReader.getAccData(a, iosAccMillis);
		iosAcc.push_back(cv::Point3f(a[0], a[1], a[2]));

		if (isiOSSuccess == false) {
			eof = false;
		}

		for (int i = 0; i < 5 && eof == true; i++) {
			isAndSuccess = androidFileReader.getAccData(a, andAccMillis);
			if (isAndSuccess == false) {
				eof = false;
			}
		}
		andAcc.push_back(cv::Point3f(a[0], a[1], a[2]));

		while (eof && iosAccMillis >= iosGyroMillis) {
			isiOSSuccess = iosFileReader.getGyroData(r, iosGyroMillis);
			
			if (isiOSSuccess == false) {
				eof = false;
			}
		}
		iosGyro.push_back(cv::Point3f(r[0], r[1], r[2]));

		while (eof && andAccMillis >= andGyroMillis) {
			isAndSuccess = androidFileReader.getGyroData(r, andGyroMillis);

			if (isAndSuccess == false) {
				eof = false;
			}
		}
		andGyro.push_back(cv::Point3f(r[0], r[1], r[2]));

		while (eof && (iosGyroMillis >= iosImageMillis || iosAccMillis >= iosImageMillis)) {
			cv::Mat iOSImage;
			isiOSSuccess = iosFileReader.getImage(iOSImage, iosImageMillis);

			if (isiOSSuccess == false) {
				eof = false;
			}
			else {
				cv::imshow("iOS Image", iOSImage);
				cv::waitKey(1);
				cout << "ios : " << iosAccMillis << ", " << iosGyroMillis << ", " << iosImageMillis << endl;
			}
		}

		while (eof && (andGyroMillis >= andImageMillis || andAccMillis >= andImageMillis)) {
			cv::Mat andImage;
			isAndSuccess = androidFileReader.getImage(andImage, andImageMillis);

			if (isAndSuccess == false) {
				eof = false;
			}
			else {
				//cv::transpose(andImage, andImage);
				//cv::flip(andImage, andImage, 1);
				cv::imshow("Android Image", andImage);
				cv::waitKey(1);
				cout << "and : " << andAccMillis << ", " << andGyroMillis << ", " << andImageMillis << endl;
			}
		}

		count++;
	}

	drawGraph(iosAcc, 0, "iosAcc");
	drawGraph(iosGyro, 1, "iosGyro");
	drawGraph(andAcc, 0, "andAcc");
	drawGraph(andGyro, 1, "andGyro");
	cv::waitKey();

	return;
}


bool drawGraph(vector<cv::Point3f> data, int type, string name, float scaleData) {
	/*ofstream file(name + ".txt");
	for (int i = 0; i < data.size(); i++) {
	file << data[i].x << " ";
	file << data[i].y << " ";
	file << data[i].z << "\n";
	}
	file.close();*/

	int guideLine = 0;
	int scale = 1;

	if (type == 0) {		//0 : 속도 가속도 등.
		guideLine = 98 * scaleData;
		scale = 10 * scaleData;
	}
	else if (type == 1) {	//1 : 각도.
		guideLine = 90 * scaleData;
		scale = 180 / CV_PI * scaleData;
	}
	else if (type == 2) {
		guideLine = 1.5 * scaleData;
		scale = 10 * scaleData;
	}

	cv::Mat graph = cv::Mat::zeros(300, data.size(), CV_32FC3);
	cv::line(graph, cv::Point2i(0, graph.rows / 2), cv::Point2i(graph.cols, graph.rows / 2), cv::Scalar(255, 255, 255), 1);
	cv::line(graph, cv::Point2i(0, graph.rows / 2 - guideLine), cv::Point2i(graph.cols, graph.rows / 2 - guideLine), cv::Scalar(10, 10, 10), 1);
	cv::line(graph, cv::Point2i(0, graph.rows / 2 + guideLine), cv::Point2i(graph.cols, graph.rows / 2 + guideLine), cv::Scalar(10, 10, 10), 1);

	for (int i = 0; i < data.size() / 400; i++) {
		cv::line(graph, cv::Point2i(i * 400, 0), cv::Point2i(i * 400, 300), cv::Scalar(255, 255, 255), 5);
	}

	for (int i = 0; i < data.size() - 1; i++) {
		cv::circle(graph, cv::Point2i(i, graph.rows / 2 - data[i].x * scale), 1, cv::Scalar(0, 0, 255), -1);
		cv::circle(graph, cv::Point2i(i, graph.rows / 2 - data[i].y * scale), 1, cv::Scalar(0, 255, 0), -1);
		cv::circle(graph, cv::Point2i(i, graph.rows / 2 - data[i].z * scale), 1, cv::Scalar(255, 0, 0), -1);
		//cv::line(graph, cv::Point2i(i, graph.rows / 2 - data[i].x * scale), cv::Point2i(i + 1, graph.rows / 2 - data[i + 1].x * scale), cv::Scalar(0, 0, 255), 1);
		//cv::line(graph, cv::Point2i(i, graph.rows / 2 - data[i].y * scale), cv::Point2i(i + 1, graph.rows / 2 - data[i + 1].y * scale), cv::Scalar(0, 255, 0), 1);
		//cv::line(graph, cv::Point2i(i, graph.rows / 2 - data[i].z * scale), cv::Point2i(i + 1, graph.rows / 2 - data[i + 1].z * scale), cv::Scalar(255, 0, 0), 1);
	}

	cv::resize(graph, graph, cv::Size(graph.size().width / 4, graph.size().height));
	cv::imshow(name, graph);
	cv::waitKey(1);

	return true;
}