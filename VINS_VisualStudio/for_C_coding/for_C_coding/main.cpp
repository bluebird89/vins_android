//#include "vld.h"

#include <iostream>
#include <fstream>

#define NOGDI
#include <Windows.h>

#include <opencv2/opencv.hpp>

#include "Utils/IMUFileReader.h"
#include "WrapperNoThreadUseMutex.hpp"

using namespace std;

string dataNum = "05";
FileType fileType = FileType::AndroidFile;

static void process(Wrapper* wrapper) {


	while (true) {
		wrapper->process();
		sleep_thread(1.f);
	}
}

void main() {
	printf("select file type(android : 0, iOS : 1) : ");
	scanf("%d", &fileType);

	char temp[50];
	printf("data num : ");
	scanf("%s", temp);
	dataNum = string(temp);

	printf("set parallax(default is 30) : ");
	scanf("%d", &parallaxThreshold);

	Wrapper* wrapper;

	string path;
	if (fileType == FileType::AndroidFile) {
		wrapper = new Wrapper(DeviceType::Android);
		path = "../../../androidData/" + dataNum;
	}
	else if (fileType == FileType::iOSFile) {
		wrapper = new Wrapper(DeviceType::iPhone6s);
		path = "../../../iOSData/" + dataNum;
	}
	else {
		wrapper = new Wrapper(DeviceType::Android);
	}

	IMUFileReader imuFileReader(path, fileType);


	int count = 0;

	long long int accMillis = -1;
	long long int gyroMillis = -1;
	long long int imageMillis = -1;

	float r[3];
	float a[3];

	bool eof = true;

	cv::Mat image;

	int key = -1;

	int reduceSensorRate = 5;

	int imageCount = 0;

	//thread processThread(process, wrapper);

	while (eof == true) {
		bool isSuccess = true;
		//for (int i = 0; i < 5; i++) {
			isSuccess = imuFileReader.getAccData(a, accMillis);
		//}

		if (isSuccess == false) {
			eof = false;
		}
		else {
			if (fileType == FileType::iOSFile) {
				a[0] = -a[0];
				a[1] = -a[1];
				a[2] = -a[2];
			}

			if (reduceSensorRate == 5) {
				wrapper->imuStartUpdate(a, accMillis, 0);
			}
		}

		while (eof == true && accMillis >= gyroMillis) {
			isSuccess = imuFileReader.getGyroData(r, gyroMillis);
			if (isSuccess == false) {
				eof = false;
			}
			else {
				if (reduceSensorRate == 5) {
					wrapper->imuStartUpdate(r, gyroMillis, 1);
					reduceSensorRate = 0;
				}
				else {
					reduceSensorRate++;
				}
				//reduceSensorRate = 5;
			}
		}

		while (eof == true && (gyroMillis >= imageMillis || accMillis >= imageMillis)) {
			if (imageMillis > 0) {
				//이미지가 센서보다 먼저 들어가면 안되서 이렇게 처리함.
				wrapper->processImage(image.data, image.cols, image.rows, imageMillis);
				wrapper->process();
				Log("####imageCount : %d", imageCount);
			}

			isSuccess = imuFileReader.getImage(image, imageMillis);
			imageCount++;
			//if (fileType == FileType::AndroidFile) {
			//	cv::transpose(image, image);
			//	cv::flip(image, image, 1);
			//}

			if (isSuccess == false) {
				eof = false;
			}
			else {
				cv::imshow("image", image);
				key = cv::waitKey(1);
				//cout << accMillis << ", " << gyroMillis << ", " << imageMillis << endl;
			}
		}

		if (key == ' ') {
			bool pause = true;
			key = -1;
			while (pause) {
#if PANGOLIN_VIEWER
				wrapper->viewer.onDraw();
#endif
				cv::imshow("image", image);
				key = cv::waitKey(10);
				 
				if (key == ' ') {
					pause = false;
					key = -1;
				}
			}
		}
		else if(key == '1') {
			wrapper->setBox();
		}

		count++;
	}

	while (key != 27) {
#if PANGOLIN_VIEWER
		wrapper->viewer.onDraw();
#endif
		key = cv::waitKey(10);
	}

	return;
}