#include <iostream>
#include <fstream>

#include <Windows.h>
#include <tchar.h>
#include <direct.h>

#include <opencv2/opencv.hpp>

#include "IMUFileReader.h"

using namespace std;

string dataNum = "05";

int DeleteAllFiles(LPCSTR szDir, int recur);

void main() {
	FileType fileType = FileType::AndroidFile;
	printf("select file type(android : 0, iOS : 1) : ");
	scanf("%d", &fileType);

	char temp[50];
	printf("data num : ");
	scanf("%s", temp);
	dataNum = string(temp);

	string path;
	if (fileType == FileType::AndroidFile) {
		path = "../../../androidData/" + dataNum;
	}
	else if (fileType == FileType::iOSFile) {
		path = "../../../iOSData/" + dataNum;
	}

	IMUFileReader imuFileReader(path, fileType);

	string outputPath = path + "/dataset-dir" + dataNum + "/";
	
	int mkdirReuslt = _mkdir(outputPath.c_str());

	if (mkdirReuslt == -1) {
		DeleteAllFiles(outputPath.c_str(), 0);
	}
	mkdirReuslt = _mkdir((outputPath + "cam0").c_str());

	ofstream csv(outputPath + "imu0.csv");

	int count = 0;

	long long int accMillis = -1;
	long long int gyroMillis = -1;
	long long int imageMillis = -1;

	float r[3];
	float a[3];

	bool eof = true;

	csv << "timestamp, omega_x, omega_y, omega_z, alpha_x, alpha_y, alpha_z" << endl;

	while (eof) {
		bool isSuccess = true;
		isSuccess = imuFileReader.getAccData(a, accMillis);

		if (isSuccess == false) {
			eof = false;
		}

		while (eof && accMillis >= gyroMillis) {
			isSuccess = imuFileReader.getGyroData(r, gyroMillis);
			if (isSuccess == false) {
				eof = false;
			}
			else {
				if (accMillis >= 1) {
					csv << accMillis << ", " << r[0] << ", " << r[1] << ", " << r[2] << ", " <<
						a[0] << ", " << a[1] << ", " << a[2] << endl;
				}
			}
		}

		while (eof && (gyroMillis >= imageMillis || accMillis >= imageMillis)) {
			cv::Mat image;
			isSuccess = imuFileReader.getImage(image, imageMillis);
			if (isSuccess == false) {
				eof = false;
			}
			else {
				cv::imwrite(outputPath + "cam0/" + to_string(imageMillis) + ".png", image);
				cv::imshow("image", image);
				cv::waitKey(1);
				cout << accMillis << ", " << gyroMillis << ", " << imageMillis << endl;
			}
		}

		count++;
	}

	cout << "file extract done" << endl;

	return;
}

int DeleteAllFiles(LPCSTR szDir, int recur) {
	HANDLE hSrch;
	WIN32_FIND_DATA wfd;
	int res = 1;

	TCHAR DelPath[MAX_PATH];
	TCHAR FullPath[MAX_PATH];
	TCHAR TempPath[MAX_PATH];

	lstrcpy(DelPath, szDir);
	lstrcpy(TempPath, szDir);
	if (lstrcmp(DelPath + lstrlen(DelPath) - 4, _T("\\*.*")) != 0)
		lstrcat(DelPath, _T("\\*.*"));

	hSrch = FindFirstFile(DelPath, &wfd);
	if (hSrch == INVALID_HANDLE_VALUE) {
		if (recur > 0)
			RemoveDirectory(TempPath);

		return -1;
	}
	while (res) {
		wsprintf(FullPath, _T("%s\\%s"), TempPath, wfd.cFileName);

		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_READONLY) {
			SetFileAttributes(FullPath, FILE_ATTRIBUTE_NORMAL);
		}

		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (lstrcmp(wfd.cFileName, _T(".")) && lstrcmp(wfd.cFileName, _T(".."))) {
				recur++;
				DeleteAllFiles(FullPath, recur);
				recur--;
			}
		}
		else {
			DeleteFile(FullPath);
		}
		res = FindNextFile(hSrch, &wfd);
	}
	FindClose(hSrch);

	if (recur > 0)
		RemoveDirectory(TempPath);

	return 0;
}