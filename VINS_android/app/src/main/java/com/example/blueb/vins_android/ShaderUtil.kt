package com.example.blueb.vins_android

import android.opengl.GLES20

/**
 * Created by blueb on 2018-04-05.
 */
object ShaderUtil {

    fun createProgram(vertexSrc: String, fragmentSrc: String): Int {
        val vertexShader = ShaderUtil.loadShader(GLES20.GL_VERTEX_SHADER, vertexSrc)
        val fragmentShader = ShaderUtil.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSrc)

        val shaderProgramId = GLES20.glCreateProgram()
        GLES20.glAttachShader(shaderProgramId, vertexShader)
        GLES20.glAttachShader(shaderProgramId, fragmentShader)
        GLES20.glLinkProgram(shaderProgramId)

        return shaderProgramId
    }

    private fun loadShader(type: Int, shaderSrc: String): Int {
        val shader: Int
        shader = GLES20.glCreateShader(type)
        GLES20.glShaderSource(shader, shaderSrc)
        GLES20.glCompileShader(shader)
        return shader
    }
}