package com.example.blueb.vins_android;

/**
 * Created by blueb on 2018-04-05.
 */

public enum ColorFormat {

    RGB888(1),
    YUV420sp(2),
    YUV420(3),
    YUV420_888(4),
    GRAY8(5);

    private int value;

    ColorFormat(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ColorFormat fromValue(int id) {
        for (ColorFormat type : ColorFormat.values()) {
            if (type.getValue() == id) {
                return type;
            }
        }
        return null;
    }
}