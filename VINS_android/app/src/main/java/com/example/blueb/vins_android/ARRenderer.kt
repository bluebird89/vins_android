package com.example.blueb.vins_android

import android.app.Activity
import android.graphics.SurfaceTexture
import android.opengl.GLES11Ext
import android.opengl.GLES20
import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import android.util.Log
import com.example.blueb.vins_android.JNIInterface.drawAR
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * Created by blueb on 2018-04-05.
 */

class ARRenderer constructor() {
    private val tag =  ARRenderer::class.java.simpleName

    private val VERTEX_SHADER_SRC =
            "attribute vec4 a_position;\n" +
                    "attribute vec2 a_texCoord;\n" +
                    "varying vec2 v_texCoord;\n" +
                    "uniform mat4 u_mvpMatrix;\n" +
                    "void main()							\n" +
                    "{										\n" +
                    "	gl_Position = u_mvpMatrix * a_position;\n" +
                    "	v_texCoord = a_texCoord; 			\n" +
                    "}										\n"

    private val FRAGMENT_SHADER_SRC = (
            //"#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +
                    "varying vec2 v_texCoord;\n" +
                    "uniform sampler2D u_texture;\n" +

                    "void main(void)\n" +
                    "{\n" +
                    "   vec4 color = texture2D(u_texture, v_texCoord);" +
                   // "   if(color.x > 0.1 || color.y > 0.1 || color.z > 0.1) {" +
                    "       color.w = 1.0;" +
                   // "   }" +
                    //"   else {" +
                    //"       color.w = 0.0;" +
                    //"   }" +
                    "   " +
                    "	gl_FragColor = color;\n" +
                    "}\n")


    private val VERTEX_BUF = floatArrayOf(
            -0.5f, -0.5f, 1.5f, // top left
            -0.5f, 0.5f, 1.5f, // bottom left
            0.5f, 0.5f, 1.5f, // bottom right
            0.5f, -0.5f, 1.5f  // top right

//            -0.25f, -0.25f, 2.0f, // top left
//            -0.25f, 0.25f, 2.0f, // bottom left
//            0.25f, 0.25f, 2.0f, // bottom right
//            0.25f, -0.25f, 2.0f  // top right
    )

    /*  private val VERTEX_BUF = floatArrayOf(-0.5f, 0.5f, 0.0f, // top left
              -0.5f, -0.5f, 0.0f, // bottom left
              0.5f, -0.5f, 0.0f, // bottom right
              0.5f, 0.5f, 0.0f  // top right
      )*/

    private val INDEX_BUF = byteArrayOf(0, 1, 2, 2, 3, 0)

    private val TEXTURE_COORD_BUF = floatArrayOf(0.0f, 0.0f,
                                                0.0f, 1.0f,
                                                1.0f, 1.0f,
                                                1.0f, 0.0f)

    private var shaderProgramId = -1
    private var positionHandle : Int? = null
    private var textureCoordHandle : Int? = null
    private var mvpMatrixHandle : Int? = null
    private var textureHandle : Int? = null


    private var vertexBuffer : FloatBuffer? = null
    private var indexBuffer : ByteBuffer? = null
    private var textureCoordBuff : FloatBuffer? = null
    private val projectionMatrix = floatArrayOf(0.0f, -2.0f, 0.0f, 0.0f, -3.711111f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0000119f, 1.0f, 0.0f, 0.0f, -0.060000356f, 0.0f)

//    private val TEXTURE_BUF = byteArrayOf(0.toByte(), 1.toByte(), 2.toByte(),
//                                          3.toByte(), 4.toByte(), 5.toByte(),
//                                          6.toByte(), 7.toByte(), 8.toByte(),
//                                          9.toByte(), 10.toByte(), 11.toByte(),
//                                          0.toByte(), 255.toByte(), 0.toByte(),
//                                          0.toByte(), 255.toByte(), 0.toByte(),
//                                          0.toByte(), 255.toByte(), 0.toByte(),
//            0.toByte(), 255.toByte(), 0.toByte(),
//                                          0.toByte(), 255.toByte(), 0.toByte())

    private val TEXTURE_BUF = byteArrayOf(255.toByte(), 255.toByte(), 255.toByte(), 0.toByte(),
                                          255.toByte(), 255.toByte(), 255.toByte(), 0.toByte(),
                                          255.toByte(), 255.toByte(), 255.toByte(), 0.toByte())

    private val drawARBuf = ByteArray(640 * 480 * 3);

    private var textureImageBuffer : ByteBuffer? = null

    private var surfaceWidth = 1280
    private var surfaceHeight = 720

    var surfaceTexture : SurfaceTexture? = null
    private var textureID : IntArray? = null

    fun getTextureID() : IntArray{
        textureID?.let{
            return it
        }?:kotlin.run {
            return intArrayOf(-1);
        }
    }

    fun initQuad() {
        textureID = IntArray(1)
        GLES20.glGenTextures(1, textureID, 0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureID!![0])
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)

        var bb = ByteBuffer.allocateDirect(VERTEX_BUF.size * java.lang.Float.SIZE / 8)
        bb.order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer()
        vertexBuffer?.put(VERTEX_BUF)
        vertexBuffer?.position(0)

        bb = ByteBuffer.allocateDirect(INDEX_BUF.size * java.lang.Byte.SIZE / 8)
        bb.order(ByteOrder.nativeOrder())
        indexBuffer = bb
        indexBuffer?.put(INDEX_BUF)
        indexBuffer?.position(0)

        bb = ByteBuffer.allocateDirect(TEXTURE_COORD_BUF.size * java.lang.Float.SIZE / 8)
        bb.order(ByteOrder.nativeOrder())
        textureCoordBuff = bb.asFloatBuffer()
        textureCoordBuff?.put(TEXTURE_COORD_BUF)
        textureCoordBuff?.position(0)

        bb = ByteBuffer.allocateDirect(640 * 480 * 3)
        bb.order(ByteOrder.nativeOrder())
        textureImageBuffer = bb
        //textureImageBuffer?.put(TEXTURE_BUF)
        //textureImageBuffer?.position(0)

        shaderProgramId = ShaderUtil.createProgram(VERTEX_SHADER_SRC, FRAGMENT_SHADER_SRC)

        positionHandle = GLES20.glGetAttribLocation(shaderProgramId, "a_position")
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramId, "a_texCoord")
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramId, "u_mvpMatrix")
        textureHandle = GLES20.glGetUniformLocation(shaderProgramId, "u_texture")
    }

    //fun drawQuad(texture: IntArray, projectionMatrix: FloatArray) {
    fun drawQuad() {
        /*if(textureID != null){
            if(cameraController.cameraSurfaceTexture !=null) {
                cameraController.cameraSurfaceTexture.updateTexImage()
            }
        }*/

        //GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        //GLES20.glViewport(0, 0, surfaceWidth, surfaceHeight)
        //GLES20.glDisable(GLES20.GL_DEPTH_TEST)
        //GLES20.glDisable(GLES20.GL_CULL_FACE)
        GLES20.glUseProgram(shaderProgramId)
       // GLES20.glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

        GLES20.glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        GLES20.glVertexAttribPointer(positionHandle!!, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer)
        GLES20.glEnableVertexAttribArray(positionHandle!!)

        GLES20.glVertexAttribPointer(textureCoordHandle!!, 2, GLES20.GL_FLOAT, false,0, textureCoordBuff)
        GLES20.glEnableVertexAttribArray(textureCoordHandle!!)

        GLES20.glUniformMatrix4fv(mvpMatrixHandle!!, 1, false, projectionMatrix, 0)

        drawAR(drawARBuf)
        textureImageBuffer!!.put(drawARBuf)
        textureImageBuffer!!.position(0)

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glUniform1i(textureHandle!!, 0)

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureID!![0])
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D,0, GLES20.GL_RGB, 640, 480, 0, GLES20.GL_RGB, GLES20.GL_UNSIGNED_BYTE, textureImageBuffer)

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, INDEX_BUF.size, GLES20.GL_UNSIGNED_BYTE, indexBuffer)

        GLES20.glDisableVertexAttribArray(positionHandle!!)
        GLES20.glDisableVertexAttribArray(textureCoordHandle!!)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)

        GLES20.glUseProgram(0)
    }
}