package com.example.blueb.vins_android

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.opengl.GLSurfaceView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import com.example.blueb.vins_android.JNIInterface.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream

class MainActivity : AppCompatActivity(), SensorEventListener {
    private val tag : String = "Sensor MainActivity"
    private val externalStoregePath = Environment.getExternalStorageDirectory().absolutePath

    private val glSurfaceView : GLSurfaceView by lazy {
        gl_surface_view as GLSurfaceView
    }
    private var cameraRenderer : CameraRenderer? = null

    private var writeFile : Boolean = false

    private var gyroFile : File? = null
    private var gyroFos : FileOutputStream? = null
    private var accFile : File? = null
    private var accFos : FileOutputStream? = null
    private var magFile : File? = null
    private var magFos : FileOutputStream? = null

    private var setTexture = false


    private lateinit var sensorManager: SensorManager
    private lateinit var cameraController : CameraController

    //private val cameraController : CameraController by lazy {
    //    CameraController.create(this)
    //}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        cameraController = CameraController.create(this)

        glSurfaceView.setEGLContextClientVersion(2)
        cameraRenderer = CameraRenderer(this, cameraController)
        glSurfaceView.setRenderer(cameraRenderer)


        if (writeFile == true) {
            reset.setOnClickListener {
                isFirst = true

                gyroFos?.close()
                accFos?.close()
                magFos?.close()

                gyroFile = File(externalStoregePath + "/gyro.txt")
                gyroFos = FileOutputStream(gyroFile)

                accFile = File(externalStoregePath + "/acc.txt")
                accFos = FileOutputStream(accFile)

                magFile = File(externalStoregePath + "/mag.txt")
                magFos = FileOutputStream(magFile)
            }
            Log.d(tag, Environment.getExternalStorageState())
            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                gyroFile = File(externalStoregePath + "/gyro.txt")
                gyroFos = FileOutputStream(gyroFile)

                accFile = File(externalStoregePath + "/acc.txt")
                accFos = FileOutputStream(accFile)

                magFile = File(externalStoregePath + "/mag.txt")
                magFos = FileOutputStream(magFile)
            }
        }

        box.setOnClickListener{
            setBox()
        }
    }

    override fun onResume() {
        super.onResume()

        val sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL)
        for(list in sensorList){
            Log.d(tag, list.name + ", " + list.type)
        }

        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST
        )
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                SensorManager.SENSOR_DELAY_FASTEST
        )
       /* sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_FASTEST
        )*/
    }

    override fun onDestroy() {
        super.onDestroy()

        gyroFos?.close()
        accFos?.close()
        magFos?.close()
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    private var timestamp : Long = 0
    private var isFirst : Boolean = true
    private var position : FloatArray = kotlin.FloatArray(3)
    private var velocity : FloatArray = kotlin.FloatArray(3)
    private var prevAcceleration : FloatArray = kotlin.FloatArray(3)
    private var rotation : FloatArray = kotlin.FloatArray(3)
    private var magnetometer : FloatArray = kotlin.FloatArray(3)

    private var reduceSensorRate : Int = 0

    //acc data rate 1/5 로 하면 좀 빠름.
    override fun onSensorChanged(p0: SensorEvent) {
        if(p0.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            var acceleration: FloatArray = kotlin.FloatArray(3)

            acceleration[0] = p0.values[0]
            acceleration[1] = p0.values[1]
            acceleration[2] = p0.values[2]

            if (isFirst) {
                timestamp = p0.timestamp
                isFirst = false
                position[0] = 0f
                position[1] = 0f
                position[2] = 0f
                velocity[0] = 0f
                velocity[1] = 0f
                velocity[2] = 0f
                prevAcceleration[0] = acceleration[0]
                prevAcceleration[1] = acceleration[1]
                prevAcceleration[2] = acceleration[2]

            } else {
                var dt: Float = (p0.timestamp.toFloat() - timestamp.toFloat()) / 1000000000f
                velocity[0] += (prevAcceleration[0] + acceleration[0]) / 2 * dt
                velocity[1] += (prevAcceleration[1] + acceleration[1]) / 2 * dt
                velocity[2] += (prevAcceleration[2] + acceleration[2]) / 2 * dt

                position[0] += velocity[0] * dt
                position[1] += velocity[1] * dt
                position[2] += velocity[2] * dt

                timestamp = p0.timestamp
                prevAcceleration[0] = acceleration[0]
                prevAcceleration[1] = acceleration[1]
                prevAcceleration[2] = acceleration[2]

            }

            accelationText.text = "acceleration \n" + acceleration.zip("XYZ".toList()).fold("") { acc, pair ->
                "$acc${pair.second}: ${pair.first}\n"
            }

           // Log.e("@@@@", "sensor time : " + p0.timestamp);

            if(writeFile == true) {
                writeAccData(acceleration, p0.timestamp)
            }

            if(reduceSensorRate == 5) {
                if (setAccData(acceleration, p0.timestamp)) {
                     //Log.d(tag, "acc time : " + p0.timestamp)
                    positionText.text ="position \n" + acceleration.zip("XYZ".toList()).fold("") { acc, pair ->
                        "$acc${pair.second}: ${pair.first}\n"
                    }
                } else {
                    Log.d(tag, "acc fail")
                }
            }

            if(setTexture == false){
                var textureID = cameraRenderer?.getTextureID()

                textureID?.let {
                    if(textureID[0] != -1){
                        cameraController.setTextureID(textureID)
                        cameraController.start(0, 640, 480)
                        setTexture = true
                    }
                }
            }

        }else if(p0.sensor.type == Sensor.TYPE_GYROSCOPE) {
            rotation[0] = p0.values[0]
            rotation[1] = p0.values[1]
            rotation[2] = p0.values[2]

            if(writeFile == true) {
                writeGyroData(rotation, p0.timestamp)
            }

            if(reduceSensorRate == 5) {
                if (setGyroData(rotation, p0.timestamp)) {
                   // Log.d(tag, "gyro time : " + p0.timestamp)
                    rotationText.text = "rotation \n"+ rotation.zip("XYZ".toList()).fold("") { acc, pair ->
                        "$acc${pair.second}: ${pair.first}\n"
                    }
                } else {
                    Log.d(tag, "gyro fail")
                }
                reduceSensorRate = 0;
            }
            else{
                reduceSensorRate++;
            }

        }else if(p0.sensor.type == Sensor.TYPE_MAGNETIC_FIELD){
            magnetometer[0] = p0.values[0]
            magnetometer[1] = p0.values[1]
            magnetometer[2] = p0.values[2]

            if(writeFile == true) {
                writeMagData(magnetometer, p0.timestamp)
            }
           // Log.d(tag, "mag success" + p0.timestamp)
        }
    }

    fun writeGyroData(rotation : FloatArray, millis : Long) {
        val buffer : String = rotation[0].toString() + " " + rotation[1].toString() + " " + rotation[2].toString() + " " + millis.toString() + "\n"
        gyroFos?.write(buffer.toByteArray())
    }

    fun writeAccData(acceleration : FloatArray, millis : Long) {
        val buffer : String = acceleration[0].toString() + " " + acceleration[1].toString() + " " + acceleration[2].toString() + " " + millis.toString() + "\n"
        accFos?.write(buffer.toByteArray())
    }

    fun writeMagData(magnetometer : FloatArray, millis : Long) {
        val buffer : String = magnetometer[0].toString() + " " + magnetometer[1].toString() + " " + magnetometer[2].toString() + " " + millis.toString() + "\n"
        magFos?.write(buffer.toByteArray())
    }
}
