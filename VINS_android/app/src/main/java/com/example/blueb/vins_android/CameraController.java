/*
 * Copyright 2016 Maxst, Inc. All Rights Reserved.
 */
package com.example.blueb.vins_android;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.Log;

import java.util.List;

public abstract class CameraController {

	private static final String TAG = CameraController.class.getSimpleName();

	static final int REQUEST_CAMERA_PERMISSION = 200;

	static CameraController create(Context context) {
		CameraController instance;

		instance = new Camera2Controller(context);

		return instance;
	}

	static void destroy(){
	}

	enum CameraState {
		None,
		Opening,
		Opened,
		Closing,
		Closed
	}

	//    CameraSurfaceView cameraSurfaceView;
	SurfaceTexture cameraSurfaceTexture;
	//    Handler cameraHandler;
	CameraState cameraState = CameraState.None;

//    void setCameraSurfaceView(CameraSurfaceView cameraSurfaceView) {
//        this.cameraSurfaceView = cameraSurfaceView;
//    }

	abstract int start(int cameraId, int width, int height);

	abstract void stop();

	abstract int getWidth();

	abstract int getHeight();

	abstract void setTextureID(int[] textureID);

	abstract boolean setFocusMode(int focusMode);

	abstract boolean setFlashLightMode(boolean toggle);

	abstract boolean setAutoWhiteBalanceLock(boolean toggle);

	abstract Object [] getParamList();

	abstract boolean setParam(String key, String value);

	CameraSize getOptimalPreviewSize(List<CameraSize> sizes, int preferredWidth, int preferredHeight) {
		double minRegion = Double.MAX_VALUE;
		CameraSize optimalSize = null;
		for (CameraSize size : sizes) {
			if (size.width <= preferredWidth && size.height <= preferredHeight) {
				if (Math.abs(size.width * size.height - preferredWidth * preferredHeight) <= minRegion) {
					minRegion = Math.abs(size.width * size.height - preferredWidth * preferredHeight);
					optimalSize = size;

					Log.i(TAG, "Optimal Preview width  : " + optimalSize.width + " height : " + optimalSize.height);
				}
			}
		}

		return optimalSize;
	}
}
