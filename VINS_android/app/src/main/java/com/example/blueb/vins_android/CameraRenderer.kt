package com.example.blueb.vins_android

import android.app.Activity
import android.graphics.SurfaceTexture
import android.opengl.GLES11Ext.GL_TEXTURE_EXTERNAL_OES
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.util.Log
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * Created by blueb on 2018-04-05.
 */

class CameraRenderer constructor(activity: Activity, cameraController: CameraController) : GLSurfaceView.Renderer {
    private val tag =  CameraRenderer::class.java.simpleName

    private var activity: Activity? = null

    private val VERTEX_SHADER_SRC =
            "attribute vec4 a_position;\n" +
            "attribute vec2 a_texCoord;\n" +
            "varying vec2 v_texCoord;\n" +
            "uniform mat4 u_mvpMatrix;\n" +
            "void main()							\n" +
            "{										\n" +
            "	gl_Position = u_mvpMatrix * a_position;\n" +
            "	v_texCoord = a_texCoord; 			\n" +
            "}										\n"

    private val FRAGMENT_SHADER_SRC = (
            "#extension GL_OES_EGL_image_external : require\n" +
            "precision mediump float;\n" +
            "varying vec2 v_texCoord;\n" +
            "uniform samplerExternalOES u_texture;\n" +

            "void main(void)\n" +
            "{\n" +
            "	gl_FragColor = texture2D(u_texture, v_texCoord);\n" +
            "}\n")


    private val VERTEX_BUF = floatArrayOf(
            -0.5f, -0.5f, 1.5f, // top left
            -0.5f, 0.5f, 1.5f, // bottom left
            0.5f, 0.5f, 1.5f, // bottom right
            0.5f, -0.5f, 1.5f  // top right
    )

  /*  private val VERTEX_BUF = floatArrayOf(-0.5f, 0.5f, 0.0f, // top left
            -0.5f, -0.5f, 0.0f, // bottom left
            0.5f, -0.5f, 0.0f, // bottom right
            0.5f, 0.5f, 0.0f  // top right
    )*/

    private val INDEX_BUF = byteArrayOf(0, 1, 2, 2, 3, 0)

    private val TEXTURE_COORD_BUF = floatArrayOf(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f)

    private var shaderProgramId = -1
    private var positionHandle : Int? = null
    private var textureCoordHandle : Int? = null
    private var mvpMatrixHandle : Int? = null
    private var textureHandle : Int? = null

    private var vertexBuffer : FloatBuffer? = null
    private var indexBuffer : ByteBuffer? = null
    private var textureCoordBuff : FloatBuffer? = null
    private val projectionMatrix = floatArrayOf(0.0f, -2.0f, 0.0f, 0.0f, -3.711111f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0000119f, 1.0f, 0.0f, 0.0f, -0.060000356f, 0.0f)

    private var surfaceWidth = 1280
    private var surfaceHeight = 720

    private var cameraController = cameraController

    var surfaceTexture : SurfaceTexture? = null
    private var textureID : IntArray? = null

    private var arRenderer = ARRenderer()

    fun getTextureID() : IntArray{
        textureID?.let{
            return it
        }?:kotlin.run {
            return intArrayOf(-1);
        }
    }

    fun initQuad() {
        textureID = IntArray(1)
        GLES20.glGenTextures(1, textureID, 0)
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, textureID!![0])
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, 0)

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
        var bb = ByteBuffer.allocateDirect(VERTEX_BUF.size * java.lang.Float.SIZE / 8)
        bb.order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer()
        vertexBuffer?.put(VERTEX_BUF)
        vertexBuffer?.position(0)

        bb = ByteBuffer.allocateDirect(INDEX_BUF.size * java.lang.Byte.SIZE / 8)
        bb.order(ByteOrder.nativeOrder())
        indexBuffer = bb
        indexBuffer?.put(INDEX_BUF)
        indexBuffer?.position(0)

        bb = ByteBuffer.allocateDirect(TEXTURE_COORD_BUF.size * java.lang.Float.SIZE / 8)
        bb.order(ByteOrder.nativeOrder())
        textureCoordBuff = bb.asFloatBuffer()
        textureCoordBuff?.put(TEXTURE_COORD_BUF)
        textureCoordBuff?.position(0)

        shaderProgramId = ShaderUtil.createProgram(VERTEX_SHADER_SRC, FRAGMENT_SHADER_SRC)

        positionHandle = GLES20.glGetAttribLocation(shaderProgramId, "a_position")
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramId, "a_texCoord")
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramId, "u_mvpMatrix")
        textureHandle = GLES20.glGetUniformLocation(shaderProgramId, "u_texture")

        arRenderer.initQuad()
    }

    fun draw(texture: IntArray, projectionMatrix: FloatArray) {
        if(textureID != null){
            if(cameraController.cameraSurfaceTexture !=null) {
                cameraController.cameraSurfaceTexture.updateTexImage()
            }
        }

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        GLES20.glViewport(0, 0, surfaceWidth, surfaceHeight)
        GLES20.glDisable(GLES20.GL_DEPTH_TEST)
        GLES20.glDisable(GLES20.GL_CULL_FACE)

        GLES20.glUseProgram(shaderProgramId)

        GLES20.glVertexAttribPointer(positionHandle!!, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer)
        GLES20.glEnableVertexAttribArray(positionHandle!!)

        GLES20.glVertexAttribPointer(textureCoordHandle!!, 2, GLES20.GL_FLOAT, false,0, textureCoordBuff)
        GLES20.glEnableVertexAttribArray(textureCoordHandle!!)

        GLES20.glUniformMatrix4fv(mvpMatrixHandle!!, 1, false, projectionMatrix, 0)

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glUniform1i(textureHandle!!, 0)
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, texture[0])

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, INDEX_BUF.size, GLES20.GL_UNSIGNED_BYTE, indexBuffer)

        GLES20.glDisableVertexAttribArray(positionHandle!!)
        GLES20.glDisableVertexAttribArray(textureCoordHandle!!)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)

        arRenderer.drawQuad()
    }

//    constructor(activity: Activity) {
//        this.activity = activity
//    }

    override fun onSurfaceCreated(p0: GL10?, p1: EGLConfig?) {
        initQuad()
    }

    override fun onSurfaceChanged(p0: GL10?, p1: Int, p2: Int) {
        surfaceWidth = p1
        surfaceHeight = p2
    }

    override fun onDrawFrame(p0: GL10?) {
        textureID?.let {
            draw(it, projectionMatrix)
        }?:run{
            Log.e(tag,"texture id 가 없음")
        }
    }
}