/*
 * Copyright 2016 Maxst, Inc. All Rights Reserved.
 */
package com.example.blueb.vins_android;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.nfc.Tag;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceHolder;

import junit.framework.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import kotlin.jvm.Synchronized;

import static android.opengl.GLES11Ext.GL_TEXTURE_EXTERNAL_OES;
import static com.example.blueb.vins_android.JNIInterface.process;
import static com.example.blueb.vins_android.JNIInterface.setImageData;
import static java.util.Arrays.asList;

@TargetApi(Build.VERSION_CODES.M)
class Camera2Controller extends CameraController /*implements SurfaceHolder.Callback*/ {
    private static final String TAG = Camera2Controller.class.getSimpleName();

    private CameraSize cameraSize = new CameraSize(0, 0);
    private byte[] yuvBuffer = null;
    private byte[] yBuffer = null;

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private boolean writeFile = false;

    private double NS2S = 1.0 / 1000000000.0;
    private double NS2MS = 1.0 / 1000000.0;

    private HandlerThread backgroundThread;
    private Handler backgroundHandler;
    private ImageReader previewReader;
    private CameraDevice cameraDevice;
    private CaptureRequest.Builder previewRequestBuilder;
    private CameraCaptureSession captureSession;
    private CaptureRequest previewRequest;
    private boolean keepSendNewFrame = false;

    private File imageFile;
    private FileOutputStream imageFos;
    private String externalStroagePath = Environment.getExternalStorageDirectory().getAbsolutePath();

    private Context context;

    private int[] textureID;

    Camera2Controller(Context context) {
        this.context = context;

        if(writeFile == true) {
            Log.d(TAG, Environment.getExternalStorageState());
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                imageFile = new File(externalStroagePath + "/imageFile.dat");
                try {
                    imageFos = new FileOutputStream(imageFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        VINSLogicThread thread = new VINSLogicThread();
        thread.start();
    }

    public void setTextureID(int[] textureID) {
        this.textureID = textureID;
    }


    private void startInternal(int id, int width, int height) {
        startBackgroundThread();

        try {
            CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            String strCameraId = manager.getCameraIdList()[id];

            CameraCharacteristics characteristics = manager.getCameraCharacteristics(strCameraId);

            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            Size[] sizes = map.getOutputSizes(SurfaceHolder.class);

            ArrayList<CameraSize> cameraSizes = new ArrayList<>();
            for (Size size : sizes) {
                cameraSizes.add(new CameraSize(size.getWidth(), size.getHeight()));
            }

            cameraSize = getOptimalPreviewSize(cameraSizes, width, height);
            Log.e(TAG, "camera size :  " + cameraSize.height + ", " + cameraSize.width);
            previewReader = ImageReader.newInstance(cameraSize.width, cameraSize.height, ImageFormat.YUV_420_888, 2);
            previewReader.setOnImageAvailableListener(mOnPreviewFrameListener, null);

            manager.openCamera(strCameraId, mStateCallback, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            cameraState = CameraState.None;
        }
    }

    @Override
    public int start(final int cameraId, final int width, final int height) {
        if (!checkPermission()) {
            return 1;
        } else {
//		cameraHandler.post(new Runnable() {
//			@Override
//			public void run() {
            if (cameraState != CameraState.None && cameraState != CameraState.Closed) {
                return 2;
            }

            cameraState = CameraState.Opening;
            startInternal(cameraId, width, height);
//			}
//		});
            return 0;
        }
    }

    @Override
    public void stop() {

        keepSendNewFrame = false;

//		cameraHandler.post(new Runnable() {
//			@Override
//			public void run() {
        if (cameraState != CameraState.Opened) {
            return;
        }

        cameraState = CameraState.Closing;

        closeCamera();
        stopBackgroundThread();
        cameraSize.width = 0;
        cameraSize.height = 0;
        if (cameraSurfaceTexture != null) {
            cameraSurfaceTexture.release();
            cameraSurfaceTexture = null;
        }

        cameraState = CameraState.Closed;
//			}
//		});
    }
    static long prev = -1;
    private final ImageReader.OnImageAvailableListener mOnPreviewFrameListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(final ImageReader reader) {

            ColorFormat colorFormat = ColorFormat.YUV420_888;

            Image acquiredImage = reader.acquireLatestImage();
            if (acquiredImage == null) {
                return;
            }

           //Log.e("MaxstAR-Native", "image time : " + (int)(acquiredImage.getTimestamp() * NS2MS));
            Assert.assertTrue("Image format is not ImageFormat.YUV_420_888", acquiredImage.getFormat() == ImageFormat.YUV_420_888);

            int width = acquiredImage.getWidth();
            int height = acquiredImage.getHeight();

            Image.Plane yPlane = acquiredImage.getPlanes()[0];
            int ySize = yPlane.getBuffer().remaining();

            //Image.Plane uPlane = acquiredImage.getPlanes()[1];
            //Image.Plane vPlane = acquiredImage.getPlanes()[2];

            // be aware that this size does not include the padding at the end, if there is any
            // (e.g. if pixel stride is 2 the size is ySize / 2 - 1)
            //int uSize = uPlane.getBuffer().remaining();
            //int vSize = vPlane.getBuffer().remaining();

            //int margin = 0;
            //if (ySize / 2 - vSize > 0) {
            //    margin = ySize / 2 - vSize;
            //}

            //if (yuvBuffer == null || yuvBuffer.length < ySize + uSize + vSize) {
            //   yuvBuffer = new byte[ySize + uSize + vSize + 100]; // Preventing overflow. 100 has no meaning.
            //}

            if (yBuffer == null || yBuffer.length < ySize) {
            	yBuffer = new byte[ySize + 100]; // Preventing overflow. 100 has no meaning.
            }
            yPlane.getBuffer().get(yBuffer, 0, ySize);

            //yPlane.getBuffer().get(yuvBuffer, 0, ySize);

            //int uvPixelStride = uPlane.getPixelStride(); //stride guaranteed to be the same for u and v planes
            //if (uvPixelStride == 1) {
             //   uPlane.getBuffer().get(yuvBuffer, ySize, uSize);
              //  vPlane.getBuffer().get(yuvBuffer, ySize + uSize, vSize);
              //  colorFormat = ColorFormat.YUV420;
           // } else {
             //   vPlane.getBuffer().get(yuvBuffer, ySize, vSize);
              //  uPlane.getBuffer().get(yuvBuffer, ySize + vSize + margin, uSize);
              //  colorFormat = ColorFormat.YUV420_888;

                // Below code consume cpu very high!!
//				vPlane.getBuffer().get(yuvBuffer, ySize, vSize);
//				for (int i = 0; i < uSize; i += 2) {
//					yuvBuffer[ySize + i + 1] = uPlane.getBuffer().get(i);
//				}
                long curr = acquiredImage.getTimestamp();
                //Log.e(TAG, "millis : " + (curr - prev));
                prev = curr;
          //  }

            if (keepSendNewFrame) {
                //CameraController.setNewCameraFrame(yuvBuffer, yuvBuffer.length, width, height, colorFormat.getValue());
                //이미지 버퍼 받기.
               // Log.d(TAG, "camera time : " + acquiredImage.getTimestamp());
                setImageData(yBuffer, width, height, acquiredImage.getTimestamp());

                if(writeFile == true) {
                    writeCameraData(yBuffer, ySize, width, height, acquiredImage.getTimestamp());
                }

                //Log.d(TAG, "image success  " + acquiredImage.getTimestamp());

            }

            acquiredImage.close();
        }
    };

    private class VINSLogicThread extends Thread{
        public VINSLogicThread(){

        }

        public void run(){
            while(true) {
                process();
                //Log.d("MaxstAR-Native", "VINSLogicThread");
                try {
                    sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeCameraData(byte[] buffer, int size, int width, int height, long millis) {
        if(buffer == null){
            return;
        }
        try {
            imageFos.write(intToByteArray(width));
            imageFos.write(intToByteArray(height));
            imageFos.write(intToByteArray(size));
            imageFos.write(longToByteArray(millis));
            imageFos.write(buffer, 0, size);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public  byte[] longToByteArray(long value) {
        byte[] byteArray = new byte[8];
        byteArray[7] = (byte)(value >> 56);
        byteArray[6] = (byte)(value >> 48);
        byteArray[5] = (byte)(value >> 40);
        byteArray[4] = (byte)(value >> 32);
        byteArray[3] = (byte)(value >> 24);
        byteArray[2] = (byte)(value >> 16);
        byteArray[1] = (byte)(value >> 8);
        byteArray[0] = (byte)(value);
        return byteArray;
    }

    public  byte[] intToByteArray(int value) {
        byte[] byteArray = new byte[4];
        byteArray[3] = (byte)(value >> 24);
        byteArray[2] = (byte)(value >> 16);
        byteArray[1] = (byte)(value >> 8);
        byteArray[0] = (byte)(value);
        return byteArray;
    }

    public  int byteArrayToInt(byte bytes[]) {
        return ((((int)bytes[3] & 0xff) << 24) |
                (((int)bytes[2] & 0xff) << 16) |
                (((int)bytes[1] & 0xff) << 8) |
                (((int)bytes[0] & 0xff)));
    }

    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
            Camera2Controller.this.cameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            cameraDevice.close();
            Camera2Controller.this.cameraDevice = null;
            cameraState = CameraState.None;
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            cameraDevice.close();
            Camera2Controller.this.cameraDevice = null;
            cameraState = CameraState.None;
        }
    };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createCameraPreviewSession() {
        try {
            if (cameraSurfaceTexture == null) {
                Log.d(TAG, "##" + textureID[0]);
                cameraSurfaceTexture = new SurfaceTexture(textureID[0]);
            }
            Log.d(TAG, "##" + textureID[0]);

            Surface surface = new Surface(cameraSurfaceTexture);
            previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewRequestBuilder.addTarget(surface);
            previewRequestBuilder.addTarget(previewReader.getSurface());
            cameraDevice.createCaptureSession(asList(surface, previewReader.getSurface()), new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (null == cameraDevice) {
                                return;
                            }
                            captureSession = cameraCaptureSession;
                            previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
//							previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO);
//							previewRequestBuilder.set(CaptureRequest.CONTROL_SCENE_MODE, CaptureRequest.CONTROL_SCENE_MODE_SPORTS);
                            previewRequest = previewRequestBuilder.build();
                            try {
                                captureSession.setRepeatingRequest(previewRequest, null, backgroundHandler);
                                cameraState = CameraState.Opened;
                                keepSendNewFrame = true;
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                                cameraState = CameraState.None;
                            }

//							cameraOpenCloseLock.release();
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
//							cameraOpenCloseLock.release();
                            cameraState = CameraState.None;
                        }
                    }, null
            );


        } catch (CameraAccessException e) {
            e.printStackTrace();
            cameraState = CameraState.None;
        }
    }

    private void closeCamera() {
        if (null != captureSession) {
            captureSession.close();
            captureSession = null;
        }

        if (null != previewReader) {
            previewReader.setOnImageAvailableListener(null, null);
            previewReader.close();
            previewReader = null;
        }

        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }

        Log.i(TAG, "Android closeCamera completed");

        yuvBuffer = null;
    }

    private void startBackgroundThread() {
        backgroundThread = new HandlerThread("CameraBackground");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
        backgroundHandler.post(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
            }
        });
    }

    private void stopBackgroundThread() {
        backgroundThread.quitSafely();
        try {
            backgroundThread.join();
            backgroundThread = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    int getWidth() {
        return cameraSize.width;
    }

    @Override
    int getHeight() {
        return cameraSize.height;
    }

    @Override
    boolean setFocusMode(int focusMode) {
        return false;
    }

    @Override
    boolean setFlashLightMode(boolean on) {
        return false;
    }

    @Override
    boolean setAutoWhiteBalanceLock(boolean toggle) {
        return false;
    }

    @Override
    Object[] getParamList() {
        return new String[0];
    }

    @Override
    boolean setParam(String key, String value) {
        return false;
    }

    private boolean checkPermission() {
        PackageManager pm = context.getPackageManager();
        int permissionIsGranted = pm.checkPermission("android.permission.CAMERA", context.getPackageName());
        return permissionIsGranted == 0;
    }
}
