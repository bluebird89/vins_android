/*
 * Copyright 2016 Maxst, Inc. All Rights Reserved.
 */
package com.example.blueb.vins_android;

class CameraSize {

	public int width;
	public int height;

	CameraSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
}
