package com.example.vins_dataload

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.opengl.GLSurfaceView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import com.example.vins_dataload.JNIInterface.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {
    private val tag : String = "Sensor MainActivity"
    private val externalStoregePath = Environment.getExternalStorageDirectory().absolutePath

    private val glSurfaceView : GLSurfaceView by lazy {
        gl_surface_view as GLSurfaceView
    }
    private var cameraRenderer : CameraRenderer? = null

    private var writeFile : Boolean = false

    private var gyroFile : File? = null
    private var gyroFos : FileInputStream? = null
    private var accFile : File? = null
    private var accFos : FileInputStream? = null
    private var magFile : File? = null
    private var magFos : FileInputStream? = null

    private var setTexture = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        glSurfaceView.setEGLContextClientVersion(2)
        cameraRenderer = CameraRenderer(this)
        glSurfaceView.setRenderer(cameraRenderer)


        if (writeFile == true) {
            reset.setOnClickListener {
                gyroFos?.close()
                accFos?.close()
                magFos?.close()

                gyroFile = File(externalStoregePath + "/gyro.txt")
                gyroFos = FileInputStream(gyroFile)

                accFile = File(externalStoregePath + "/acc.txt")
                accFos = FileInputStream(accFile)

                magFile = File(externalStoregePath + "/mag.txt")
                magFos = FileInputStream(magFile)
            }
            Log.d(tag, Environment.getExternalStorageState())
            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                gyroFile = File(externalStoregePath + "/gyro.txt")
                gyroFos = FileInputStream(gyroFile)

                accFile = File(externalStoregePath + "/acc.txt")
                accFos = FileInputStream(accFile)

                magFile = File(externalStoregePath + "/mag.txt")
                magFos = FileInputStream(magFile)
            }
        }

        box.setOnClickListener{
            setBox()
        }

        setPath(externalStoregePath)

        var thread = VINSLogicThread()
        thread.start()
    }

    private inner class VINSLogicThread : Thread() {

        override fun run() {
            var r = kotlin.FloatArray(3)
            var a = kotlin.FloatArray(3)
            var accMillis = kotlin.LongArray(1)
            var gyroMillis = kotlin.LongArray(1)
            var imageMillis = kotlin.LongArray(1)
            imageMillis[0] = -1
            var imageBuffer = kotlin.ByteArray(640*480)

            var count = 0

            var eof = true

            var reduceSensorRate = 0

            while(eof) {
                var isSuccess = true
                isSuccess = getAccData(a, accMillis)

                if(isSuccess == false){
                    eof = false
                }
                else {
                    if(reduceSensorRate == 5){
                        setAccData(a, accMillis[0]);
                    }
                }

                while(eof == true && accMillis[0] >= gyroMillis[0]){
                    isSuccess = getGyroData(r, gyroMillis)
                    if(isSuccess == false){
                        eof = false
                    }
                    else {
                        if(reduceSensorRate == 5){
                            setGyroData(r, gyroMillis[0])
                        }
                        else {
                            reduceSensorRate++
                        }
                    }
                }

                while(eof == true && (gyroMillis[0] >= imageMillis[0] || accMillis[0] >= imageMillis[0])) {
                    if(imageMillis[0] > 0) {
                        setImageData(imageBuffer, 640, 480, imageMillis[0])
                        process()
                        Log.e("tag","process + " + count)
                    }
                    getImageData(imageBuffer, imageMillis)
                }
                count++

                try {
                    Thread.sleep(1)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()

        gyroFos?.close()
        accFos?.close()
        magFos?.close()
    }

    private var timestamp : Long = 0
    private var position : FloatArray = kotlin.FloatArray(3)
    private var velocity : FloatArray = kotlin.FloatArray(3)
    private var prevAcceleration : FloatArray = kotlin.FloatArray(3)
    private var rotation : FloatArray = kotlin.FloatArray(3)
    private var magnetometer : FloatArray = kotlin.FloatArray(3)


    fun readGyroData(rotation : FloatArray, millis : Long) {
        val buffer : String = rotation[0].toString() + " " + rotation[1].toString() + " " + rotation[2].toString() + " " + millis.toString() + "\n"
        gyroFos?.read(buffer.toByteArray())
    }

    fun readAccData(acceleration : FloatArray, millis : Long) {
        val buffer : String = acceleration[0].toString() + " " + acceleration[1].toString() + " " + acceleration[2].toString() + " " + millis.toString() + "\n"
        accFos?.read(buffer.toByteArray())
    }
}
