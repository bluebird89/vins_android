package com.example.vins_dataload;

public class JNIInterface {
    static {
        System.loadLibrary("native-lib");
    }

    static native boolean setImageData(byte[] buffer, int width, int height, long millis);
    static native boolean setGyroData(float[] pose, long millis);
    static native boolean setAccData(float[] pose, long millis);
    static native boolean process();
    static native boolean init();
    static native boolean setPath(String path);
    static native boolean getImageData(byte[] buffer, long[] millis);
    static native boolean getGyroData(float[] pose, long[] millis);
    static native boolean getAccData(float[] pose, long[] millis);

    static native boolean drawAR(byte[] buffer);
    static native boolean setBox();
}
