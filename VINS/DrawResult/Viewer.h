/*==============================================================================
Copyright 2017 Maxst, Inc. All Rights Reserved.
==============================================================================*/

#pragma once

#include <Eigen/Eigen>
#include <pangolin/pangolin.h>

using namespace std;

class Viewer
{
public:
	Viewer();
	~Viewer();

	void initialize();
	void onSizeChanged(int width, int height);
	bool onDraw();
	void onTimer(int value);
	void onKeyPressed(unsigned char key, int x, int y);
	void onMouseClicked(int button, int state, int x, int y);

	void setMappoints(map<int, Eigen::Vector3f>* points);
	void setCameraPose(Eigen::Matrix4f pose);
protected:
	int m_viewerWidth;
	int m_viewerHeight;
	float m_viewpointX;
	float m_viewpointY;
	float m_viewpointZ;
	float m_viewpointF;

	pangolin::OpenGlRenderState *s_cam;
	pangolin::View *d_cam;
	pangolin::Var<bool> *menuFollowCamera;
	pangolin::Var<bool> *menuShowPoints;
	pangolin::Var<bool> *menuShowKeyFrames;
	pangolin::Var<bool> *menuShowGraph;
	pangolin::Var<bool> *menuPause;
	pangolin::OpenGlMatrix Twc;
	bool m_follow = true;

	map<int, Eigen::Vector3f> mapPoints;
	vector<Eigen::Matrix4f> cameraPose;

	void drawCurrentCamera(pangolin::OpenGlMatrix &Twc);
	void drawKeyframes(const bool bDrawKF, const bool bDrawGraph);
	void drawMappoints();
	void drawCoordinates();
};

