/*==============================================================================
Copyright 2017 Maxst, Inc. All Rights Reserved.
==============================================================================*/


#include "Viewer.h"

Viewer::Viewer()
{
}

Viewer::~Viewer()
{
}

void Viewer::initialize()
{
	m_viewerWidth = 1024;
	m_viewerHeight = 768;

	m_viewpointX = 0;
	m_viewpointY = 0.7;
	m_viewpointZ = 2.0;
	m_viewpointF = 500;

	m_follow = true;

	pangolin::CreateWindowAndBind("MAXSTAR SLAM+", m_viewerWidth, m_viewerHeight);

	// 3D Mouse handler requires depth testing to be enabled
	glEnable(GL_DEPTH_TEST);

	// Issue specific OpenGl we might need
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	pangolin::CreatePanel("menu").SetBounds(0.0, 1.0, 0.0, pangolin::Attach::Pix(150));
	menuFollowCamera = new pangolin::Var<bool>("menu.Follow Camera", false, true);
	menuShowPoints = new pangolin::Var<bool>("menu.Show Points", true, true);
	menuShowKeyFrames = new pangolin::Var<bool>("menu.Show KeyFrames", true, true);
	menuShowGraph = new pangolin::Var<bool>("menu.Show Graph", true, true);
	menuPause = new pangolin::Var<bool>("menu.Pause", false, true);

	// Define Camera Render Object (for view / scene browsing)
	s_cam = new pangolin::OpenGlRenderState(
		pangolin::ProjectionMatrix(m_viewerWidth, m_viewerHeight, m_viewpointF, m_viewpointF, m_viewerWidth / 2.0, m_viewerWidth/2.0, 0.1, 1000),
		pangolin::ModelViewLookAt(m_viewpointX, m_viewpointY, m_viewpointZ, 0, 0, 0, 0.0, 1.0, 0.0)
		);

	// Add named OpenGL viewport to window and provide 3D Handler
	d_cam = &(pangolin::CreateDisplay()
		.SetBounds(0.0, 1.0, pangolin::Attach::Pix(175), 1.0, -(float)m_viewerWidth / m_viewerHeight)
		.SetHandler(new pangolin::Handler3D(*s_cam)));

	Twc.SetIdentity();
	cameraPose.clear();
}

void Viewer::drawCurrentCamera(pangolin::OpenGlMatrix &Twc)
{
	const float &w = 0.08;
	const float h = w*1.25;
	const float z = -w*1.0;

	glPushMatrix();

#ifdef HAVE_GLES
	glMultMatrixf(Twc.m);
#else
	glMultMatrixd(Twc.m);
#endif

	glLineWidth(3);
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(w, h, z);
	glVertex3f(0, 0, 0);
	glVertex3f(w, -h, z);
	glVertex3f(0, 0, 0);
	glVertex3f(-w, -h, z);
	glVertex3f(0, 0, 0);
	glVertex3f(-w, h, z);

	glVertex3f(w, h, z);
	glVertex3f(w, -h, z);

	glVertex3f(-w, h, z);
	glVertex3f(-w, -h, z);

	glVertex3f(-w, h, z);
	glVertex3f(w, h, z);

	glVertex3f(-w, -h, z);
	glVertex3f(w, -h, z);
	glEnd();

	glPopMatrix();
}

void Viewer::drawKeyframes(const bool bDrawKF, const bool bDrawGraph)
{
	const float &w = 0.05;
	const float h = w*1.25;
	const float z = -w*1.0;

	if (bDrawKF)
	{
		for (int i = 0; i < cameraPose.size(); i++) {
			glPushMatrix();
			glMultMatrixf(cameraPose[i].data());

			glLineWidth(1);
			glColor3f(1.0f, 0.0f, 0.0f);
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(w, h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, h, z);

			glVertex3f(w, h, z);
			glVertex3f(w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(-w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(w, h, z);

			glVertex3f(-w, -h, z);
			glVertex3f(w, -h, z);
			glEnd();

			glPopMatrix();
		}

		/*std::set<maxstAR::Frame*> &overlap = m_processor->slamLogic.m_vstracker->m_k2fTracker->overlap_set_;
		for (std::set<maxstAR::Frame*>::iterator it = overlap.begin(); it != overlap.end(); ++it)
		{
			maxstAR::Frame* pKF = *it;
			Eigen::Matrix4d Twc = pKF->T_f_w_.inverse().matrix();
			Eigen::Matrix4f Twc_f;
			for (int r = 0; r < 4; r++)
				for (int c = 0; c < 4; c++)
					Twc_f(r, c) = Twc(r, c);

			glPushMatrix();

			glMultMatrixf(Twc_f.data());

			glLineWidth(1);
			glColor3f(1.0f, 0.0f, 0.0f);
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(w, h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, h, z);

			glVertex3f(w, h, z);
			glVertex3f(w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(-w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(w, h, z);

			glVertex3f(-w, -h, z);
			glVertex3f(w, -h, z);
			glEnd();

			glPopMatrix();
		}

		std::set<maxstAR::Frame*> &close = m_processor->slamLogic.m_vstracker->m_k2fTracker->close_set_;
		for (std::set<maxstAR::Frame*>::iterator it = close.begin(); it != close.end(); ++it)
		{
			maxstAR::Frame* pKF = *it;

			if (overlap.find(pKF) != overlap.end())
				continue;

			Eigen::Matrix4d Twc = pKF->T_f_w_.inverse().matrix();
			Eigen::Matrix4f Twc_f;
			for (int r = 0; r < 4; r++)
				for (int c = 0; c < 4; c++)
					Twc_f(r, c) = Twc(r, c);

			glPushMatrix();

			glMultMatrixf(Twc_f.data());

			glLineWidth(1);
			glColor3f(0.7f, 0.7f, 0.7f);
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(w, h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, h, z);

			glVertex3f(w, h, z);
			glVertex3f(w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(-w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(w, h, z);

			glVertex3f(-w, -h, z);
			glVertex3f(w, -h, z);
			glEnd();

			glPopMatrix();
		}

		std::vector<maxstAR::Frame*> &all = m_processor->slamLogic.getMap()->getFrames();
		for (size_t i = 0; i<all.size(); i++)
		{
			maxstAR::Frame* pKF = all[i];

			if (close.find(pKF) != close.end())
				continue;

			Eigen::Matrix4d Twc = pKF->T_f_w_.inverse().matrix();
			Eigen::Matrix4f Twc_f;
			for (int r = 0; r < 4; r++)
				for (int c = 0; c < 4; c++)
					Twc_f(r, c) = Twc(r, c);

			glPushMatrix();

			glMultMatrixf(Twc_f.data());

			glLineWidth(1);
			glColor3f(0.0f, 0.0f, 1.0f);
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(w, h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, -h, z);
			glVertex3f(0, 0, 0);
			glVertex3f(-w, h, z);

			glVertex3f(w, h, z);
			glVertex3f(w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(-w, -h, z);

			glVertex3f(-w, h, z);
			glVertex3f(w, h, z);

			glVertex3f(-w, -h, z);
			glVertex3f(w, -h, z);
			glEnd();

			glPopMatrix();
		}*/
	}

//	if (bDrawGraph)
//	{
//		glLineWidth(1);
//		glColor4f(0.0f, 1.0f, 0.0f, 0.6f);
//		glBegin(GL_LINES);
//
//		std::vector<maxstAR::Frame*> &all = m_processor->slamLogic.getMap()->getFrames();
//		for (size_t i = 0; i<all.size(); i++)
//		{
//			maxstAR::Frame* pKF = all[i];
//			Eigen::Vector3d Ow = pKF->T_f_w_.inverse().translation();
//
///*
//			// Covisibility Graph
//			std::vector<maxstAR::Frame*> vCovKFs = pKF->getCovisiblesByWeight(50);
//			if (!vCovKFs.empty())
//			{
//				for (vector<maxstAR::Frame*>::const_iterator vit = vCovKFs.begin(), vend = vCovKFs.end(); vit != vend; vit++)
//				{
//					//if ((*vit)->mnId<vpKFs[i]->mnId)
//					//	continue;
//					Eigen::Vector3d Ow2 = (*vit)->T_f_w_.inverse().translation();
//					glVertex3f(Ow[0], Ow[1], Ow[2]);
//					glVertex3f(Ow2[0], Ow2[1], Ow2[2]);
//				}
//			}
//*/
//
//			// Spanning tree
//			maxstAR::Frame* pParent = pKF->getParent();
//			if (pParent)
//			{
//				Eigen::Vector3d Owp = pParent->T_f_w_.inverse().translation();
//				glVertex3f(Ow[0], Ow[1], Ow[2]);
//				glVertex3f(Owp[0], Owp[1], Owp[2]);
//			}
///*
//			// Loops
//			set<KeyFrame*> sLoopKFs = vpKFs[i]->GetLoopEdges();
//			for (set<KeyFrame*>::iterator sit = sLoopKFs.begin(), send = sLoopKFs.end(); sit != send; sit++)
//			{
//				if ((*sit)->mnId<vpKFs[i]->mnId)
//					continue;
//				cv::Mat Owl = (*sit)->GetCameraCenter();
//				glVertex3f(Ow.at<float>(0), Ow.at<float>(1), Ow.at<float>(2));
//				glVertex3f(Owl.at<float>(0), Owl.at<float>(1), Owl.at<float>(2));
//			}
//*/
//		}
//
//		glEnd();
//	}
}

void Viewer::drawMappoints()
{
	glPointSize(2);
	glBegin(GL_POINTS);

	glColor3f(1.0, 1.0, 1.0);
	//auto iterPoints = mapPoints->begin();
	//for (auto iterPoints = mapPoints->begin(); iterPoints != mapPoints->end(); iterPoints++) {
	for (auto iterPoints : mapPoints) {
		glVertex3f(iterPoints.second.x(), iterPoints.second.y(), iterPoints.second.z());
	}
	
	/*for (int i = 0; i < mapPoints.size(); i++)
		glVertex3f(mapPoints[i][0], mapPoints[i][1], mapPoints[i][2]);*/

	/*glColor3f(0.0, 0.0, 0.0);
	std::vector<Eigen::Vector3d> &others = m_processor->slamLogic.m_vstracker->m_k2fTracker->m_other_points;
	for (int i = 0; i < others.size(); i++)
		glVertex3f(others[i][0], others[i][1], others[i][2]);

	glColor3f(0.7, 0.7, 0.7);
	std::vector<Eigen::Vector3d> &closed = m_processor->slamLogic.m_vstracker->m_k2fTracker->m_close_points;
	for (int i = 0; i < closed.size(); i++)
		glVertex3f(closed[i][0], closed[i][1], closed[i][2]);

	glColor3f(1.0, 1.0, 0.0);
	std::vector<Eigen::Vector3d> &overlap = m_processor->slamLogic.m_vstracker->m_k2fTracker->m_overlap_points;
	for (int i = 0; i < overlap.size(); i++)
		glVertex3f(overlap[i][0], overlap[i][1], overlap[i][2]);*/

	glEnd();
/*
	glPointSize(2);
	glBegin(GL_POINTS);
	glColor3f(1.0, 0.0, 0.0);

	for (set<MapPoint*>::iterator sit = spRefMPs.begin(), send = spRefMPs.end(); sit != send; sit++)
	{
		if ((*sit)->isBad())
			continue;
		cv::Mat pos = (*sit)->GetWorldPos();
		glVertex3f(pos.at<float>(0), pos.at<float>(1), pos.at<float>(2));

	}

	glEnd();
*/
}

void Viewer::drawCoordinates() {
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0, 0, 0);
	glVertex3f(1, 0, 0);

	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 1, 0);

	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 1);

	glEnd();
}

bool Viewer::onDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//maxstAR::Frame *f = m_processor->slamLogic.m_vstracker->getPrevFrame();
	//if (f != nullptr)
	//{
	//	//mpMapDrawer->GetCurrentOpenGLCameraMatrix(Twc);
	//	Sophus::SE3 T_w_f = f->T_f_w_.inverse();
	//	Eigen::Matrix4d M = T_w_f.matrix();
	//	for (int i = 0; i < 4; i++)
	//		for (int j = 0; j < 4; j++)
	//			Twc(i, j) = M(i, j);
	//}
	//
	if (menuFollowCamera->Get() && m_follow)
	{
		s_cam->Follow(Twc);
	}
	else if (menuFollowCamera->Get() && !m_follow)
	{
		s_cam->SetModelViewMatrix(pangolin::ModelViewLookAt(m_viewpointX, m_viewpointY, m_viewpointZ, 0, 0, 0, 0.0, -1.0, 0.0));
		s_cam->Follow(Twc);
		m_follow = true;
	}
	else if (!menuFollowCamera->Get() && m_follow)
	{
		m_follow = false;
	}

	d_cam->Activate(*s_cam);
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

	//mpMapDrawer->DrawCurrentCamera(Twc);
	if (cameraPose.size() > 0) {
		Twc = pangolin::OpenGlMatrix::ColMajor4x4(cameraPose[cameraPose.size() - 1].data());
	}
	drawCoordinates();
	drawKeyframes(*menuShowKeyFrames, *menuShowGraph);
	drawCurrentCamera(Twc);
	drawMappoints();
	
	pangolin::FinishFrame();
/*
	cv::Mat im = mpFrameDrawer->DrawFrame();
	cv::imshow("ORB-SLAM2: Current Frame", im);
	cv::waitKey(mT);

	if (menuReset)
	{
		menuShowGraph = true;
		menuShowKeyFrames = true;
		menuShowPoints = true;
		menuLocalizationMode = false;
		if (bLocalizationMode)
			mpSystem->DeactivateLocalizationMode();
		bLocalizationMode = false;
		bFollow = true;
		menuFollowCamera = true;
		mpSystem->Reset();
		menuReset = false;
	}

	if (Stop())
	{
		while (isStopped())
		{
			usleep(3000);
		}
	}

	if (CheckFinish())
		break;
*/
	return pangolin::ShouldQuit();
}

void Viewer::setMappoints(map<int, Eigen::Vector3f>* points) {
	//mapPoints = points;
	/*pangolin::OpenGlMatrix rot = pangolin::OpenGlMatrix::RotateX(-M_PI / 2);
	Eigen::Matrix4f p;
	for (int i = 0; i < 16; i++) {
		p.data()[i] = rot.m[i];
	}
	Eigen::Matrix3f a;
	a = p.block<3, 3>(0, 0);*/

	/*for (int i = 0; i < points.size(); i++) {
		mapPoints.push_back(points[i]);
	}*/
	mapPoints = *points;
}

void Viewer::setCameraPose(Eigen::Matrix4f pose) {
	/*pangolin::OpenGlMatrix rot = pangolin::OpenGlMatrix::RotateX(-M_PI / 2);
	Eigen::Matrix4f p;
	for (int i = 0; i < 16; i++) {
		p.data()[i] = rot.m[i];
	}*/

	cameraPose.push_back(pose);
}

void Viewer::onTimer(int value)
{
}

void Viewer::onKeyPressed(unsigned char key, int x, int y)
{
}

void Viewer::onMouseClicked(int button, int state, int x, int y)
{
}