//
//  global_param.h
//  VINS_ios
//
//  Created by HKUST Aerial Robotics on 2016/10/20.
//  Copyright © 2017 HKUST Aerial Robotics. All rights reserved.
//

#ifndef global_param_h
#define global_param_h

enum DeviceType
{
    iPhone7P,
    iPhone7,
    iPhone6sP,
    iPhone6s,
    iPadPro97,
    iPadPro129,
    unDefine,
	Android
};

#define IOS_DATA 1


#define MIN_LOOP_NUM 22
#define LOOP_FREQ 3
#define WINDOW_SIZE 10

#if IOS_DATA
#define DATA_POOL_SIZE 10
#else
#define DATA_POOL_SIZE 10
#endif

#define PNP_SIZE 6
#define SIZE_POSE 7
#define SIZE_SPEEDBIAS 9
#define SIZE_SPEED 3
#define SIZE_BIAS  6

#define SIZE_FEATURE 1

#define NUM_OF_F 1000
#define NUM_OF_CAM 1
#define DEBUG_MODE false
#define C_PI 3.1415926

#define GRAVITY ((double)9.805)
#define BIAS_ACC_THRESHOLD ((double)0.5)
#define BIAS_GYR_THRESHOLD ((double)0.1)
#define G_THRESHOLD ((double)3.0)
#define G_NORM ((double)9.805)
#define INIT_KF_THRESHOLD ((double)18)
#define SFM_R_THRESHOLD ((double)180)
#define MAX_FEATURE_CNT 150

extern int parallaxThreshold;

extern double FOCUS_LENGTH_Y;
extern double PY;
extern double FOCUS_LENGTH_X;
extern double PX;
extern double SOLVER_TIME;
extern int FREQ;

extern int imageWidth;
extern int imageHeight;

//sensor default data
//#if IOS_DATA
//#define ACC_N ((double)0.5)  //0.02
//#define ACC_W ((double)0.002)
//#define GYR_N ((double)0.2)  //0.02
//#define GYR_W ((double)4.0e-5)
//#define SENSOR_RATE ((double)100.0)
//#else
//#define ACC_N ((double)0.017548)  //0.02
//#define ACC_W ((double)1.827e-4)
//#define GYR_N ((double)6.891e-5)  //0.02
//#define GYR_W ((double)9.931e-6)
////#define ACC_N ((double)0.0007848)  //0.02
////#define ACC_W ((double)0.0001827)
////#define GYR_N ((double)0.0006981 )  //0.02
////#define GYR_W ((double)9.931e-6)
//#define SENSOR_RATE ((double)500.0)
//#endif

extern double ACC_N;
extern double ACC_W;
extern double GYR_N;
extern double GYR_W;
extern double SENSOR_RATE;

//extrinsic param
//extrinsic param
//#if IOS_DATA
//#define RIC_y ((double)0.0)
//#define RIC_p ((double)0.0)
//#define RIC_r ((double)180.0)
//#else
//#define RIC_y ((double)-90.0)
//#define RIC_p ((double)0.0)
//#define RIC_r ((double)180.0)
//#endif

extern double RIC_y;
extern double RIC_p;
extern double RIC_r;

extern double TIC_X;
extern double TIC_Y;
extern double TIC_Z;
/* IMU
 Z^
 |   /Y
 |  /
 | /
 |/--------->X
 
 */
enum StateOrder
{
    O_P = 0,
    O_R = 3,
    O_V = 6,
    O_BA = 9,
    O_BG = 12
};

bool setGlobalParam(DeviceType device);

#if 1
#define TS(name) int64 t_##name = cv::getTickCount()
#define TE(name) printf("TIMER_" #name ": %.2fms\n", \
1000.*((cv::getTickCount() - t_##name) / cv::getTickFrequency()))
#else
#define TS(name)
#define TE(name)
#endif

#endif /* global_param_h */

