#pragma once

#include <iostream>
#include <string>
#include <fstream>

#ifdef WIN32
#include <GL/glew.h>
#include <GL/glut.h>
#endif

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#include <Eigen/Eigen>

#include "GLUtil.h"

using namespace std;

class Shader{
public:
	Shader(){};
	Shader(string v, string f){
		init(v, f);
	};
	void init(string v, string f, bool isPath = true){
		vertexshader = loadShader(GL_VERTEX_SHADER, v, isPath);
		fragmentshader = loadShader(GL_FRAGMENT_SHADER, f, isPath);

		shaderId = glCreateProgram();
		
		glAttachShader(shaderId, vertexshader);
		glAttachShader(shaderId, fragmentshader);
		glLinkProgram(shaderId);
	};

	//직접 string shader를 넣고 싶으면 path = false 로 입력 받으면 됨.
	int loadShader(GLenum type, string filename, bool isPath = true) {
		GLuint shader;
		GLint compiled;

		shader = glCreateShader(type);
		if (shader == 0){
			cout << "Shader 생성 오류" << endl;
			return 0;
		}

		string shaderCode;

		if (isPath == true) {
			ifstream shaderStream(filename, ios::in);
			if (!shaderStream.is_open()) {
				cout << filename << " 파일 읽기 오류" << endl;
				return 0;
			}
			string Line = "";
			while (getline(shaderStream, Line)) {
				shaderCode += "\n" + Line;
			}
			shaderStream.close();
		}
		else {
			shaderCode = filename;
		}

		const char* code = shaderCode.c_str();

		glShaderSource(shader, 1, &code, NULL);
		glCompileShader(shader);
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
		if (!compiled){
			GLint infoLen = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

			if (infoLen > 1){
				char* infoLog = new char[infoLen];
				glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
				cout << "Shader compile 오류 " << infoLog << endl;
				delete infoLog;
			}
			glDeleteShader(shader);
			return 0;
		}
		return shader;
	};

	GLuint getShaderId(){
		return shaderId;
	};
	void bind(){
		glUseProgram(shaderId);
	};
	void unbind(){
		glUseProgram(0);
	};
	~Shader(){
		glDetachShader(shaderId, vertexshader);
		glDetachShader(shaderId, fragmentshader);

		glDeleteShader(vertexshader);
		glDeleteShader(fragmentshader);
		glDeleteProgram(shaderId);
	};
private:
	GLuint shaderId;
	GLuint vertexshader;
	GLuint fragmentshader;
};

