attribute vec3 vertexModel;

uniform mat4 uMVPMatrix;

attribute vec2 vertexUV;

varying vec2 UV;

void main() 
{
	gl_Position = uMVPMatrix * vec4(vertexModel, 1.0);
	UV = vertexUV;
}