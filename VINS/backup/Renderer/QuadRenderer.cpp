#include "QuadRenderer.h"

#include "Logger.h"

float quadVertex[12] = {
	-320.f, 240.0f, 0.0f,
	-320.f, -240.0f, 0.0f,
	320.f, 240.0f, 0.0f,
	320.f, -240.0f, 0.0f
};
float quadUV[8] = {
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f
};


QuadRenderer::QuadRenderer() {
}


QuadRenderer::~QuadRenderer() {
}

bool QuadRenderer::init(string vshader, string fshader, bool isPath) {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	shader.init(vshader, fshader, isPath);

	vertexID = glGetAttribLocation(shader.getShaderId(), "vertexModel");
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 12, quadVertex, GL_STATIC_DRAW);
	glEnableVertexAttribArray(vertexID);

	/*int error = glGetError();
	if (error != GL_NO_ERROR) {
		cout << "error code0 : " << error << endl;
	}*/

	uvID = glGetAttribLocation(shader.getShaderId(), "vertexUV");
	glGenBuffers(1, &uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8, quadUV, GL_STATIC_DRAW);
	glEnableVertexAttribArray(uvID);

	uMVPMatrixID = glGetUniformLocation(shader.getShaderId(), "uMVPMatrix");

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	textureID = glGetUniformLocation(shader.getShaderId(), "mytexture");

	//glUniform1i(textureID, 0);
	
	cout << "shader id : " << shader.getShaderId() <<
		"\nvertex id : " << vertexID <<
		"\nuv id : " << uvID <<
		"\ntexture id : " << textureID <<
		"\nuMVPMatrix id : " << uMVPMatrixID << endl;

	ProjectionMatrix.setIdentity();
	ModelMatrix.setIdentity();
	ViewMatrix.setIdentity();
	MVPMatrix.setIdentity();

	//GLUtil::GLPerspective(ProjectionMatrix, 60, (float)1280.f / 720.f, 0.1f, 5000);
	GLUtil::GLOrtho(ProjectionMatrix, -320.f, 320.f, -240.f, 240.f, -1000.f, 1000.f);
	GLUtil::GLLookAt(ViewMatrix, 0, 0, 700, 0, 0, 100, 0, 1, 0);
	
	return true;
}

void QuadRenderer::display(int width, int height, unsigned char* image, int imageFormat) {
	Log("width %d, height %d");

	shader.bind();
	MVPMatrix = ProjectionMatrix * ViewMatrix * ModelMatrix;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUniformMatrix4fv(uMVPMatrixID, 1, GL_FALSE, (float*)MVPMatrix.data());

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(vertexID, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glVertexAttribPointer(uvID, 2, GL_FLOAT, GL_TRUE, 0, (void*)0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	if (imageFormat == 0) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else if (imageFormat == 1) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, image);
	}
	else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	}
	glUniform1i(textureID, 0);

	//glutPostRedisplay();

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	shader.unbind();
}

void QuadRenderer::setTransform(Eigen::Matrix4f transformMatrix) {
	ModelMatrix = transformMatrix;
}
