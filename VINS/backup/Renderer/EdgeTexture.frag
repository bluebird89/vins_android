precision mediump float;
uniform sampler2D mytexture;

varying vec2 UV;

void main()
{
	 vec4 temp = texture2D(mytexture, UV);
	 if(temp[1] > 0.8){
		gl_FragColor = vec4(0.4509, 1.0, 0.9176, temp[0]);
	 }else{
		gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
	 }
	 gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
}