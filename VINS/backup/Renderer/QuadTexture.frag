precision mediump float;
uniform sampler2D mytexture;

varying vec2 UV;

mat3 sx = mat3(
	1.0, 2.0, 1.0,
	0.0, 0.0, 0.0,
	-1.0, -2.0, -1.0
);

mat3 sy = mat3(
	1.0, 0.0, -1.0,
	2.0, 0.0, -2.0,
	1.0, 0.0, -1.0
);

float imageWidth = 640.f;
float imageHeight = 480.f;

void main()
{
	 //vec4 temp = texture2D(mytexture, UV);
	 //gl_FragColor = texture2D(mytexture, UV);

	 vec3 color[9];

	 for(int i = -1; i < 2; i++){
		for(int j = -1; j < 2; j++){
			float ii = float(i);
			float jj = float(j);

			color[(i + 1) * 3 + (j + 1)] = texture2D(mytexture, vec2(UV.x + ii / imageWidth, UV.y + jj / imageHeight)).rgb;
		}
	 }

	 mat3 gray;

	 for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			gray[i][j] = color[i * 3 + j].r * 0.2126 + color[i * 3 + j].g * 0.7152 + color[i * 3 + j].b * 0.0722;
		}
	 }

	 mat3 gx = matrixCompMult(sx, gray);
	 mat3 gy = matrixCompMult(sy, gray);

	 float sumGx = 0.0f;
	 float sumGy = 0.0f;

	 for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			sumGx += gx[i][j];
			sumGy += gy[i][j];
		}
	 }

	 //float g = sqrt(pow(sumGx, 2.0f) + pow(sumGy, 2.0f));
	 float g = abs(sumGx) + abs(sumGy);
	 if(g > 0.2f){
		vec4 tempFragColor = texture2D(mytexture, UV);
		gl_FragColor = vec4(0.4509, 1.0, 0.9176, 1.0) * g + tempFragColor * (1.0f - g);
	 }else{
		gl_FragColor = texture2D(mytexture, UV);
	 }
	 //		gl_FragColor = texture2D(mytexture, UV);
}