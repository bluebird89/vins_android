#pragma once

#include "Shader.h"

class QuadRenderer
{
public:
	QuadRenderer();
	~QuadRenderer();

	bool init(string vshader, string fshader, bool isPath = true);
	void display(int width, int height, unsigned char* image, int imageFormat);
	void setTransform(Eigen::Matrix4f transformMatrix);

	Eigen::Matrix4f ModelMatrix;
	Eigen::Matrix4f ViewMatrix;
	Eigen::Matrix4f ProjectionMatrix;
	Eigen::Matrix4f MVPMatrix;

private:
	Shader shader;

	GLuint vertexID;
	GLuint vertexBuffer;

	GLuint uvID;
	GLuint uvBuffer;

	GLuint uMVPMatrixID;

	GLuint texture;
	GLuint textureID;

	int numOfPoints;
};

