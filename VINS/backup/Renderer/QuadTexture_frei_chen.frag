precision mediump float;
uniform sampler2D mytexture;

varying vec2 UV;

float imageWidth = 640.f;
float imageHeight = 480.f;

uniform mat3 G[9] = mat3[](
	1.0/(2.0*1.41421) * mat3( 1.0, 1.41421, 1.0, 0.0, 0.0, 0.0, -1.0, -1.41421, -1.0 ),
	1.0/(2.0*1.41421) * mat3( 1.0, 0.0, -1.0, 1.41421, 0.0, -1.41421, 1.0, 0.0, -1.0 ),
	1.0/(2.0*1.41421) * mat3( 0.0, -1.0, 1.41421, 1.0, 0.0, -1.0, -1.41421, 1.0, 0.0 ),
	1.0/(2.0*1.41421) * mat3( 1.41421, -1.0, 0.0, -1.0, 0.0, 1.0, 0.0, 1.0, -1.41421 ),
	1.0/2.0 * mat3( 0.0, 1.0, 0.0, -1.0, 0.0, -1.0, 0.0, 1.0, 0.0 ),
	1.0/2.0 * mat3( -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, -1.0 ),
	1.0/6.0 * mat3( 1.0, -2.0, 1.0, -2.0, 4.0, -2.0, 1.0, -2.0, 1.0 ),
	1.0/6.0 * mat3( -2.0, 1.0, -2.0, 1.0, 4.0, 1.0, -2.0, 1.0, -2.0 ),
	1.0/3.0 * mat3( 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
);

/*uniform mat3 G[9] = mat3[](
	1.0/(2.0*sqrt(2.0)) * mat3( 1.0, sqrt(2.0), 1.0, 0.0, 0.0, 0.0, -1.0, -sqrt(2.0), -1.0 ),
	1.0/(2.0*sqrt(2.0)) * mat3( 1.0, 0.0, -1.0, sqrt(2.0), 0.0, -sqrt(2.0), 1.0, 0.0, -1.0 ),
	1.0/(2.0*sqrt(2.0)) * mat3( 0.0, -1.0, sqrt(2.0), 1.0, 0.0, -1.0, -sqrt(2.0), 1.0, 0.0 ),
	1.0/(2.0*sqrt(2.0)) * mat3( sqrt(2.0), -1.0, 0.0, -1.0, 0.0, 1.0, 0.0, 1.0, -sqrt(2.0) ),
	1.0/2.0 * mat3( 0.0, 1.0, 0.0, -1.0, 0.0, -1.0, 0.0, 1.0, 0.0 ),
	1.0/2.0 * mat3( -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, -1.0 ),
	1.0/6.0 * mat3( 1.0, -2.0, 1.0, -2.0, 4.0, -2.0, 1.0, -2.0, 1.0 ),
	1.0/6.0 * mat3( -2.0, 1.0, -2.0, 1.0, 4.0, 1.0, -2.0, 1.0, -2.0 ),
	1.0/3.0 * mat3( 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
);*/

void main()
{
	 vec3 color[9];

	 for(int i = -1; i < 2; i++){
		float ii = float(i);

		for(int j = -1; j < 2; j++){
			float jj = float(j);

			color[(i + 1) * 3 + (j + 1)] = texture2D(mytexture, vec2(UV.x + ii / imageWidth, UV.y + jj / imageHeight)).rgb;
		}
	 }

	 mat3 gray;

	 for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			gray[i][j] = color[i * 3 + j].r * 0.2126 + color[i * 3 + j].g * 0.7152 + color[i * 3 + j].b * 0.0722;
		}
	 }

	 float cnv[9];

	 for (int i=0; i<9; i++) {
		float dp3 = dot(G[i][0], gray[0]) + dot(G[i][1], gray[1]) + dot(G[i][2], gray[2]);
		cnv[i] = dp3 * dp3; 
	}

	float M = (cnv[0] + cnv[1]) + (cnv[2] + cnv[3]);
	float S = (cnv[4] + cnv[5]) + (cnv[6] + cnv[7]) + (cnv[8] + M); 
	
	float result = sqrt(M/S);

	if(result > 0.1f){
		vec4 tempFragColor = texture2D(mytexture, UV);
		result *= 3.0f;
		gl_FragColor = vec4(0.4509, 1.0, 0.9176, 1.0) * result + tempFragColor * (1.0f - result);
	}else{
		gl_FragColor = texture2D(mytexture, UV);
	}
}