#pragma  once
/**
* Created by Nick on 2017-01-18.
* 좌표계 변환은 Touch_GL_Camera Coordinate Definition.png 파일을 참조.
*/
#include <Eigen/Eigen>

#define PI 3.1415926535897932384626433832795f

class GLUtil
{
public:
	static float calcVectorLength(float x, float y, float z)
	{
		return (float)sqrtf(x * x + y * y + z * z);
	}

	static void GLTranslate(Eigen::Matrix4f &result, float sx, float sy, float sz)
	{
		Eigen::Matrix4f transMat;
		transMat <<
			1.0f, 0.0f, 0.0f, sx,
			0.0f, 1.0f, 0.0f, sy,
			0.0f, 0.0f, 1.0f, sz,
			0.0f, 0.0f, 0.0f, 1.0f;

		result = transMat * result;
	}

	static void GLScale(Eigen::Matrix4f &result, float sx, float sy, float sz)
	{
		result(0, 0) *= sx;
		result(0, 1) *= sx;
		result(0, 2) *= sx;
		result(0, 3) *= sx;

		result(1, 0) *= sy;
		result(1, 1) *= sy;
		result(1, 2) *= sy;
		result(1, 3) *= sy;

		result(2, 0) *= sz;
		result(2, 1) *= sz;
		result(2, 2) *= sz;
		result(2, 3) *= sz;
	}

	static void GLRotate(Eigen::Matrix4f &result, float angle, float x, float y, float z)
	{
		float sinAngle, cosAngle;
		float mag = sqrtf(x * x + y * y + z * z);

		sinAngle = sinf(angle * PI / 180.0f);
		cosAngle = cosf(angle * PI / 180.0f);

		if (mag > 0.0f) {
			float xx, yy, zz, xy, yz, zx, xs, ys, zs;
			float oneMinusCos;

			x /= mag;
			y /= mag;
			z /= mag;

			xx = x * x;
			yy = y * y;
			zz = z * z;
			xy = x * y;
			yz = y * z;
			zx = z * x;
			xs = x * sinAngle;
			ys = y * sinAngle;
			zs = z * sinAngle;

			oneMinusCos = 1.0f - cosAngle;

			Eigen::Matrix4f rotMat;

			rotMat <<
				(oneMinusCos * xx) + cosAngle, (oneMinusCos * xy) + zs, (oneMinusCos * zx) - ys, 0.0f,
				(oneMinusCos * xy) - zs, (oneMinusCos * yy) + cosAngle, (oneMinusCos * yz) + xs, 0.0f,
				(oneMinusCos * zx) + ys, (oneMinusCos * yz) - xs, (oneMinusCos * zz) + cosAngle, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f;

			result = rotMat * result;
		}
	}

	static void GLRotateEuler(Eigen::Matrix4f &result, float x, float y, float z) {
		x *= PI / 180.0f;
		y *= PI / 180.0f;
		z *= PI / 180.0f;
		float cx = cosf(x);
		float sx = sinf(x);
		float cy = cosf(y);
		float sy = sinf(y);
		float cz = cosf(z);
		float sz = sinf(z);
		float cxsy = cx * sy;
		float sxsy = sx * sy;

		Eigen::Matrix4f rotMat;

		rotMat <<
			cy * cz, cxsy * cz + cx * sz, -sxsy * cz + sx * sz, 0.0f,
			-cy * sz, -cxsy * sz + cx * cz, sxsy * sz + sx * cz, 0.0f,
			sy, -sx * cy, cx * cy, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f;

		result = rotMat * result;
	}

	static void GLOrtho(Eigen::Matrix4f &result,
		float left, float right, float bottom, float top, float nearZ, float farZ)
	{
		float deltaX = right - left;
		float deltaY = top - bottom;
		float deltaZ = farZ - nearZ;

		if ((deltaX == 0.0f) || (deltaY == 0.0f) || (deltaZ == 0.0f))
			return;

		result.setIdentity();

		result(0, 0) = 2.0f / deltaX;
		result(0, 3) = -(right + left) / deltaX;
		result(1, 1) = 2.0f / deltaY;
		result(1, 3) = -(top + bottom) / deltaY;
		result(2, 2) = -2.0f / deltaZ;
		result(2, 3) = -(nearZ + farZ) / deltaZ;
	}

	static void GLPerspective(Eigen::Matrix4f &result,
		float fovy, float aspect, float zNear, float zFar)
	{
		float f = 1.0f / tanf(fovy * (PI / 360.0));
		float rangeReciprocal = 1.0f / (zNear - zFar);

		result.setIdentity();

		result(0, 0) = f / aspect;
		result(1, 0) = 0.0f;
		result(2, 0) = 0.0f;
		result(3, 0) = 0.0f;

		result(0, 1) = 0.0f;
		result(1, 1) = f;
		result(2, 1) = 0.0f;
		result(3, 1) = 0.0f;

		result(0, 2) = 0.0f;
		result(1, 2) = 0.0f;
		result(2, 2) = (zFar + zNear) * rangeReciprocal;
		result(3, 2) = -1.0f;

		result(0, 3) = 0.0f;
		result(1, 3) = 0.0f;
		result(2, 3) = 2.0f * zFar * zNear * rangeReciprocal;
		result(3, 3) = 0.0f;
	}

	static void GLLookAt(Eigen::Matrix4f &result, float eyeX, float eyeY, float eyeZ,
		float centerX, float centerY, float centerZ,
		float upX, float upY, float upZ)
	{
		// See the OpenGL GLUT documentation for gluLookAt for a description
		// of the algorithm. We implement it in a straightforward way:
		float fx = centerX - eyeX;
		float fy = centerY - eyeY;
		float fz = centerZ - eyeZ;

		// Normalize f
		float rlf = 1.0f / calcVectorLength(fx, fy, fz);
		fx *= rlf;
		fy *= rlf;
		fz *= rlf;

		// compute s = f x up (x means "cross product")
		float sx = fy * upZ - fz * upY;
		float sy = fz * upX - fx * upZ;
		float sz = fx * upY - fy * upX;

		// and normalize s
		float rls = 1.0f / calcVectorLength(sx, sy, sz);
		sx *= rls;
		sy *= rls;
		sz *= rls;

		// compute u = s x f
		float ux = sy * fz - sz * fy;
		float uy = sz * fx - sx * fz;
		float uz = sx * fy - sy * fx;

		result.setIdentity();

		float tx = -(sx * eyeX + sy * eyeY + sz * eyeZ);
		float ty = -(ux * eyeX + uy * eyeY + uz * eyeZ);
		float tz = (fx * eyeX + fy * eyeY + fz * eyeZ);

		result <<
			sx, sy, sz, tx,
			ux, uy, uz, ty,
			-fx, -fy, -fz, tz,
			0.0f, 0.0f, 0.0f, 1.0f;
	}

	//screen width, screen height, render width, render height, input x, input y, result x, result y
	//render width -> 1pixel = openGL 1 in the openGL coordinate system.
	//ex)render width = 720 -> x data = -360~+360 in the openGL coordinate system.
	static void convertCoordinateTouchScreenToOpenGL(int sw, int sh, int rw, int rh, int ix, int iy, float* rx, float* ry){
		float wr = (float)sw / rw;
		float hr = (float)sh / rh;

		int cx = sw / 2;
		int cy = sh / 2;

		//frame의 위 아래가 잘릴 경우.
		//Case of 
		if (wr > hr){
			float convertSW = sw / wr;
			float convertSH = sh / wr;

			float diffH = rh - convertSH;

			*rx = (float)ix - cx;
			*ry = (float)cy - iy;

			*rx = *rx / wr;
			*ry = *ry / wr;
		}
		//frame의 양 옆이 잘릴 경우.
		else{
			float convertSW = sw / hr;
			float convertSH = sh / hr;

			float diffW = rw - convertSW;

			*rx = (float)ix - cx;
			*ry = (float)cy - iy;

			*rx = *rx / hr;
			*ry = *ry / hr;
		}
	}

	//image width, image height, input x, input y, result x, result y
	//image size와 openGL 좌표계는 같아야함.
	//ex)image width = 1280 -> opengl 좌표계의 x 값 = -640~+640
	static void convertCoordinateOpenGLToOpenCV(
		int imw, int imh, int ix, int iy, int* rx, int* ry, bool isPortrait = true){

		int ix2 = -iy;
		int iy2 = ix;
		int cx = imw / 2;
		int cy = imh / 2;

		if (!isPortrait){
			ix2 = ix;
			iy2 = iy;
		}

		*rx = ix2 + cx;
		*ry = cy - iy2;
	}

	static void convertCoordinateOpenCVToOpenGL(
		int imw, int imh, int ix, int iy, int* rx, int* ry, bool isPortrait = true){

		int ix2 = -iy;
		int iy2 = ix;
		int cx = imw / 2;
		int cy = imh / 2;

		if (!isPortrait){
			ix2 = ix;
			iy2 = iy;
			cx = -cx;
		}

		*rx = ix2 + cx;
		*ry = cy - iy2;
	}

	//get a Matrix that is Touch Coordinate to OpenGL Coordinate.
	//deviceState 0 is Landscape Left,
	//			  1 is Portrait Up,
	//			  2 is Landscape Right,
	//			  3 is Portrait Down.
	static void convertTouchToGLMatrix3x3(Eigen::Matrix3f& result,
		int screenWidth, int screenHeight, int renderWidth, int renderHeight, int deviceState = 0){

		float widthRatio = (float)screenWidth / renderWidth;
		float heightRatio = (float)screenHeight / renderHeight;
		float divideRatio = heightRatio;

		int centerX = screenWidth / 2;
		int centerY = screenHeight / 2;

		if (widthRatio > heightRatio){
			divideRatio = widthRatio;
		}

		result <<
			1 / divideRatio, 0, -centerX / divideRatio,
			0, -1 / divideRatio, centerY / divideRatio,
			0, 0, 1;
	}

	//get a Matrix that is OpenGL Coordinate to Image Coordinate.
	//deviceState 0 is Landscape Left,
	//			  1 is Portrait Up,
	//			  2 is Landscape Right,
	//			  3 is Portrait Down.
	static void convertGLToImageMatrix3x3(Eigen::Matrix3f& result,
		int renderWidth, int renderHeight, int imageWidth, int imageHeight, int deviceState = 0){

		int tImageWidth = imageWidth;
		int tImageHeight = imageHeight;

		if (deviceState == 1 || deviceState == 3){
			tImageWidth = imageHeight;
			tImageHeight = imageWidth;
		}

		float widthRatio = renderWidth / tImageWidth;
		float heightRatio = renderHeight / tImageHeight;
		float divideRatio = heightRatio;

		int centerX = renderWidth / 2;
		int centerY = renderHeight / 2;

		if (widthRatio > heightRatio){
			divideRatio = widthRatio;
		}

		switch (deviceState){
		case 0:
			result <<
				1 / divideRatio, 0, centerX / divideRatio,
				0, -1 / divideRatio, centerY / divideRatio,
				0, 0, 1;
			break;

		case 1:
			result <<
				0, -1 / divideRatio, centerY / divideRatio,
				-1 / divideRatio, 0, centerX / divideRatio,
				0, 0, 1;
			break;

		case 2:
			result <<
				-1 / divideRatio, 0, centerX / divideRatio,
				0, 1 / divideRatio, centerY / divideRatio,
				0, 0, 1;
			break;

		case 3:
			result <<
				0, 1 / divideRatio, centerY / divideRatio,
				1 / divideRatio, 0, centerX / divideRatio,
				0, 0, 1;
			break;

		default:
			//error..or case 0.
			break;
		}
	}
};