//
// Created by blueb on 2018-02-26.
//
#include <cstdint>
#include "jni.h"

#include "WrapperNoThread.hpp"

extern "C"
{
    //Wrapper* wrapper = new Wrapper();

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_init(JNIEnv *env, jclass obj) {

        //if(wrapper == nullptr) {
        //    wrapper = new Wrapper();
        //}

        return true;// sensorIntegration->initData();
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_setGyroData(JNIEnv *env, jclass obj,
                                                                     jfloatArray inputPose,
                                                                     jlong inputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(inputPose, 0);

        if (nativeFloat == nullptr) {
            return false;
        }

        //wrapper->imuStartUpdate(nativeFloat, inputMillis, 1);

        env->ReleasePrimitiveArrayCritical(inputPose, nativeFloat, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_setAccData(JNIEnv *env, jclass obj,
                                                                    jfloatArray inputPose,
                                                                    jlong inputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(inputPose, 0);

        if (nativeFloat == nullptr) {
            return false;
        }

        //wrapper->imuStartUpdate(nativeFloat, inputMillis, 0);

        env->ReleasePrimitiveArrayCritical(inputPose, nativeFloat, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_setImageData(JNIEnv *env, jclass obj,
                                                                 jbyteArray buffer, jint width, jint height,
                                                                 jlong inputMillis) {

        jbyte *nativeChar = (jbyte *) env->GetPrimitiveArrayCritical(buffer, 0);

        if (nativeChar == nullptr) {
            return false;
        }

        //wrapper->processImage((unsigned char*)nativeChar, width, height, inputMillis);
		//wrapper->process();
		env->ReleasePrimitiveArrayCritical(buffer, nativeChar, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_drawQuad(JNIEnv *env, jclass obj) {
        
        //wrapper->drawQuad();
        
        return true;// sensorIntegration->initData();
    }
}
