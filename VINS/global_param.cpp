//
//  global_param.cpp
//  VINS_ios
//
//  Created by HKUST Aerial Robotics on 2017/05/09.
//  Copyright © 2017 HKUST Aerial Robotics. All rights reserved.
//

#include <stdio.h>
#include "global_param.hpp"

#include "Utils/Logger.h"

double FOCUS_LENGTH_Y;
double PY;
double FOCUS_LENGTH_X;
double PX;
double SOLVER_TIME;
int FREQ;
int imageWidth = 480;
int imageHeight = 640;
int parallaxThreshold = 100;

//sensor default data
double ACC_N = 0.5;
double ACC_W = 0.002;
double GYR_N = 0.2;
double GYR_W = 4.0e-5;
double SENSOR_RATE = 100;

//extrinsic param
double RIC_y = 0.0;
double RIC_p = 0.0;
double RIC_r = 180.0;

double TIC_X;
double TIC_Y;
double TIC_Z;

double NS2S = 1.0 / 1000000000.0;
double NS2MS = 1.0 / 1000000.0;

bool setGlobalParam(DeviceType device)
{
    switch (device) {
        case iPhone7P:
            LogE("Device iPhone7 plus param\n");
            FOCUS_LENGTH_X = 526.600;
            FOCUS_LENGTH_Y = 526.678;
            PX = 243.481;
            PY = 315.280;
            
            SOLVER_TIME = 0.06;
            FREQ = 3;
            
            TIC_X = 0.0;
            TIC_Y = 0.092;
            TIC_Z = 0.01;
            return true;
            break;
            
        case iPhone7:
			LogE("Device iPhone7 param\n");
            FOCUS_LENGTH_X = 526.958;
            FOCUS_LENGTH_Y = 527.179;
            PX = 244.473;
            PY = 313.844;
            SOLVER_TIME = 0.06;
            FREQ = 3;
            
            //extrinsic param
            TIC_X = 0.0;
            TIC_Y = 0.092;
            TIC_Z = 0.01;
            return true;
            break;
            
        case iPhone6s:
			LogE("Device iPhone6s param\n");
            FOCUS_LENGTH_Y = 549.477;
            PY = 320.379;
            FOCUS_LENGTH_X = 548.813;
            PX = 238.520;
            SOLVER_TIME = 0.06;
            FREQ = 3;
            
            //extrinsic param
            TIC_X = 0.0;
            TIC_Y = 0.065;
            TIC_Z = 0.0;
            return true;
            break;
            
        case iPhone6sP:
			LogE("Device iPhone6sP param\n");
            FOCUS_LENGTH_X = 547.565;
            FOCUS_LENGTH_Y = 547.998;
            PX = 239.033;
            PY = 309.452;
            
            SOLVER_TIME = 0.06;
            FREQ = 3;
            
            //extrinsic param
            TIC_X = 0.0;
            TIC_Y = 0.065;
            TIC_Z = 0.0;
            return true;
            break;
            
        case iPadPro97:
			LogE("Device ipad97 param\n");
            FOCUS_LENGTH_X = 547.234;
            FOCUS_LENGTH_Y = 547.464;
            PX = 241.549;
            PY = 317.957;
            
            SOLVER_TIME = 0.06;
            FREQ = 3;
            
            //extrinsic param
            TIC_X = 0.0;
            TIC_Y = 0.092;
            TIC_Z = 0.1;
            return true;
            break;
            
        case iPadPro129:
			LogE("Device iPad129 param\n");
            FOCUS_LENGTH_X = 547.234;
            FOCUS_LENGTH_Y = 547.464;
            PX = 241.549;
            PY = 317.957;
            
            SOLVER_TIME = 0.06;
            FREQ = 3;
            
            //extrinsic param
            TIC_X = 0.0;
            TIC_Y = 0.092;
            TIC_Z = 0.1;
            return true;
            break;

        case unDefine:
			LogE("Device unDefine");
            return false;
            break;

		case Android:
			LogE("Device Android param\n");

			//s8
			FOCUS_LENGTH_Y = 518.404;
			PY = 215.503;
			FOCUS_LENGTH_X = 523.770;
			PX = 323.308;
			//FOCUS_LENGTH_Y = 545.453;
			//PY = 246.322;
			//FOCUS_LENGTH_X = 547.553;
			//PX = 330.956;
			//g7
			/*FOCUS_LENGTH_Y = 454.511;
			PY = 235.110;
			FOCUS_LENGTH_X = 456.091;
			PX = 334.715;*/
			RIC_y = -90.0;
			RIC_p = 0.0;
			RIC_r = 180.0;
			imageWidth = 640;
			imageHeight = 480;

			SOLVER_TIME = 0.06;
			//SOLVER_TIME = 0.20;
			FREQ = 3;

			ACC_N = 0.5;
			ACC_W = 0.002;
			GYR_N = 0.2;
			GYR_W = 4.0e-5;

			SENSOR_RATE = 100;

			//extrinsic param
			TIC_X = 0.0;
			//TIC_Y = 0.065;
			//TIC_Y = 0.0;
			//TIC_Z = 0.13;
			TIC_Y = 0.13;
			TIC_Z = 0.0;

			return true;
			break;

        default:
			LogE("Device default");
            return false;
            break;
    }
}
