//
// Created by blueb on 2018-02-26.
//
#include <cstdint>
#include "jni.h"

#include "WrapperNoThreadUseMutex.hpp"
#include "Utils/Statistics.h"

extern "C"
{
    Wrapper* wrapper = new Wrapper(DeviceType::Android);

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_init(JNIEnv *env, jclass obj) {

		if(wrapper == nullptr) {
           wrapper = new Wrapper(DeviceType::Android);
        }

        return true;// sensorIntegration->initData();
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_setGyroData(JNIEnv *env, jclass obj,
                                                                     jfloatArray inputPose,
                                                                     jlong inputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(inputPose, 0);

        if (nativeFloat == nullptr) {
            return false;
        }
        wrapper->imuStartUpdate(nativeFloat, inputMillis, 1);
        env->ReleasePrimitiveArrayCritical(inputPose, nativeFloat, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_setAccData(JNIEnv *env, jclass obj,
                                                                    jfloatArray inputPose,
                                                                    jlong inputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(inputPose, 0);

        if (nativeFloat == nullptr) {
            return false;
        }
		wrapper->imuStartUpdate(nativeFloat, inputMillis, 0);
        env->ReleasePrimitiveArrayCritical(inputPose, nativeFloat, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_setImageData(JNIEnv *env, jclass obj,
                                                                 jbyteArray buffer, jint width, jint height,
                                                                 jlong inputMillis) {

        jbyte *nativeChar = (jbyte *) env->GetPrimitiveArrayCritical(buffer, 0);

        if (nativeChar == nullptr) {
            return false;
        }
		
        TS(time_processImage);
        wrapper->processImage((unsigned char*)nativeChar, width, height, inputMillis);
        TE(time_processImage);
        
		env->ReleasePrimitiveArrayCritical(buffer, nativeChar, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_process(JNIEnv *env, jclass obj) {

        TS(time_process);

		wrapper->process();
        TE(time_process);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_blueb_vins_1android_JNIInterface_drawAR(JNIEnv *env, jclass obj, 
															jbyteArray buffer) {

		jbyte* nativeChar = (jbyte*)env->GetPrimitiveArrayCritical(buffer, 0);

		if (nativeChar == nullptr) {
			return false;
		}

		bool result = wrapper->getDrawARImage((unsigned char*)nativeChar);

		env->ReleasePrimitiveArrayCritical(buffer, nativeChar, JNI_ABORT);

		return result;
    }

	JNIEXPORT jboolean JNICALL
		Java_com_example_blueb_vins_1android_JNIInterface_setBox(JNIEnv *env, jclass obj) {

		bool result = wrapper->setBox();

		return result;
	}
}
