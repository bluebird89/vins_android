/*==============================================================================
Copyright 2017 Maxst, Inc. All Rights Reserved.
==============================================================================*/


#pragma once

#include <string>
#include <map>
#include <vector>
#include <list>

#define RESET_FRAME_COUNT	30
#define FPS_FRAME_COUNT		10

enum TIME_PERIOD
{
	PERIOD_TOTAL,
	PERIOD_GOOD_FEATURE_TO_TRACK,
	PERIOD_OPTICAL_FLOW,

	PERIOD_PROCESS_IMAGE,
	PERIOD_VINS_PROCESS_IMAGE,
	PERIOD_VINS_PROCESS_IMU,

	PERIOD_PROCESSING,
	PERIOD_VINS_PNP,

	PERIOD_SOLVE_CERES,
	PERIOD_SOLVE_CERES_BEFORE_SOLVER,
	PERIOD_SOLVE_CERES_SOLVER,
	PERIOD_SOLVE_CERES_AFTER_SOLVER,
	PERIOD_SOLVE_CERES_MARGINALIZATION,

	PERIOD_DRAWAR,

	PERIOD_NUM
};

enum FPS_TYPE
{
	FPS_LOGIC = 0,
	FPS_RENDERER,
};

struct TimerElement
{
	long long startTick;
	double totalTime;
	std::vector<double> aveTimeList;
	int frameCount;

	TimerElement() : startTick(0), totalTime(0.0), frameCount(0) {}
};

struct FpsElement
{
	std::list<unsigned long long> stampList;
};

extern const char* timerPeriodName[];

class CriticalSection;

class Statistics
{
private:
	static std::map<TIME_PERIOD, TimerElement> m_elementList;
	static std::map<FPS_TYPE, FpsElement> m_fpsList;
	static unsigned long long m_startTick;

protected:
	Statistics(void);
	~Statistics(void);

public:
	static void StartTimer(TIME_PERIOD id);
	static void StopTimer(TIME_PERIOD id);
	static void Add(TIME_PERIOD id, double count);
	static void Print(TIME_PERIOD id, bool printWhenReset);
	static void ExportToFile(std::string fileName);
	static void Reset();
	static double GetAveTime(TIME_PERIOD id);
	static double GetCurTime(TIME_PERIOD id);

	static void fps(FPS_TYPE id);
	static double getFps(FPS_TYPE id);

	static void StartTimer();
	static double StopTimer();
};
