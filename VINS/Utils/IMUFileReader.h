#pragma once
#include <string>
#include <opencv2/opencv.hpp>

#define GRAVITY ((double)9.805)

using namespace std;

enum FileType
{
	AndroidFile,
	iOSFile
};

class IMUFileReader
{
public:
	IMUFileReader(string path, FileType type) {
		fileType = type;

		imageFile = fopen((path + "/imageFile.dat").c_str(), "rb");

		if (fileType == AndroidFile) {
			accFile = fopen((path + "/acc.txt").c_str(), "r");
			gyroFile = fopen((path + "/gyro.txt").c_str(), "r");
		}
		else if (fileType == iOSFile) {
			accFile = fopen((path + "/acc.dat").c_str(), "rb");
			gyroFile = fopen((path + "/gyro.dat").c_str(), "rb");
		}
	}

	bool getImage(cv::Mat& result, long long int& imageMillis) {
		if (imageFile != NULL && !feof(imageFile)) {
			fread(&width, sizeof(int), 1, imageFile);
			fread(&height, sizeof(int), 1, imageFile);
			fread(&size, sizeof(int), 1, imageFile);
			fread(&imageMillis, sizeof(long long int), 1, imageFile);

			result = cv::Mat(height, width, CV_8UC1);

			fread(result.data, sizeof(char) * size, 1, imageFile);

			return true;
		}
		else {
			return false;
		}
	}

	bool getAccData(float* a, long long int& accMillis) {
		if (accFile != NULL && !feof(accFile)) {
			if (fileType == AndroidFile) {
				fscanf(accFile, "%f %f %f %lld", a, a + 1, a + 2, &accMillis);
			}
			else if (fileType == iOSFile) {
				fread(a, sizeof(float), 3, accFile);
				fread(&accMillis, sizeof(long long int), 1, accFile);
				a[0] *= GRAVITY;
				a[1] *= GRAVITY;
				a[2] *= GRAVITY;
			}

			return true;
		}
		else {
			return false;
		}

	}

	bool getGyroData(float* r, long long int& gyroMillis) {
		if (gyroFile != NULL && !feof(gyroFile)) {
			if (fileType == AndroidFile) {
				fscanf(gyroFile, "%f %f %f %lld", r, r + 1, r + 2, &gyroMillis);
			}
			else if (fileType == iOSFile) {
				fread(r, sizeof(float), 3, gyroFile);
				fread(&gyroMillis, sizeof(long long int), 1, gyroFile);
			}

			return true;
		}
		else {
			return false;
		}
	}


private:
	FileType fileType;
	FILE *imageFile;
	FILE *gyroFile;
	FILE *accFile;

	int width = -1;
	int height = -1;
	int size = -1;
	long long int imageMillis = -1;

	float a[3];
	long long int accMillis = -1;

	float r[3];
	long long int gyroMillis = -1;
};