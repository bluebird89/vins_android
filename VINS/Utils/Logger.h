/*==============================================================================
Copyright 2017 Maxst, Inc. All Rights Reserved.
==============================================================================*/

#pragma once

#ifdef __ANDROID__
#include <android/log.h>
#ifndef LOG_TAG
#define  LOG_TAG "MaxstAR-Native"
#endif

#ifdef DEBUG
//#error DEBUG is defined
#define LogD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
//#error DEBUG is not defined
#define LogD(...) 
#endif
#define Log(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LogE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#elif defined(WIN32) || defined(__MacOS__)
extern void Log(const char* szFormat, ...);
extern void LogD(const char* szFormat, ...);
#define LogE(...)
#elif defined(__IOS__)
#include <stdio.h>
#define LogD(...)
#define Log(...) {printf(__VA_ARGS__);  printf("\n");}
#define LogE(...) {printf(__VA_ARGS__);  printf("\n");}
#else
#define LogD(...)
#define Log(...) printf(__VA_ARGS__);
#define LogE(...)
#endif
