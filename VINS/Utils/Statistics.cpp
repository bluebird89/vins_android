/*==============================================================================
Copyright 2017 Maxst, Inc. All Rights Reserved.
==============================================================================*/


#include "Statistics.h"
#include <ctime>
#include <fstream>

#include <opencv2/opencv.hpp>

#include "Utils/Logger.h"

using namespace std;

const char* timerPeriodName[PERIOD_NUM] =
{
	"TOTAL",
	"GOOD_FEATURE_TO_TRACK",
	"OPTICAL_FLOW",

	"PROCESS_IMAGE",
	"VINS_PROCESS_IMAGE",
	"VINS_PROCESS_IMU",

	"PROCESSING",
	"VINS_PNP",

	"SOLVE_CERES",
	"SOLVE_CERES_BEFORE_SOLVER",
	"SOLVE_CERES_SOLVER",
	"SOLVE_CERES_AFTER_SOLVER",
	"SOLVE_CERES_MARGINALIZATION",

	"DRAWAR"
};

map<TIME_PERIOD, TimerElement> Statistics::m_elementList;
map<FPS_TYPE, FpsElement> Statistics::m_fpsList;
unsigned long long Statistics::m_startTick;

Statistics::Statistics(void)
{
}

Statistics::~Statistics(void)
{
}

void Statistics::StartTimer(TIME_PERIOD id)
{
	map<TIME_PERIOD, TimerElement>::iterator i = m_elementList.find(id);
	if (i == m_elementList.end())
	{
		m_elementList[id] = TimerElement();
		i = m_elementList.find(id);
	}

	i->second.startTick = cv::getTickCount();
}

void Statistics::StopTimer(TIME_PERIOD id)
{
	map<TIME_PERIOD, TimerElement>::iterator i = m_elementList.find(id);
	if (i == m_elementList.end())
	{
		return;
	}
	TimerElement &te = i->second;
	te.totalTime += (double)(cv::getTickCount() - te.startTick) / cv::getTickFrequency() * 1000;
	te.frameCount++;

	if (te.frameCount % RESET_FRAME_COUNT == 0)
	{
		te.aveTimeList.push_back(te.totalTime / RESET_FRAME_COUNT);
		te.frameCount = 0;
		te.totalTime = 0.0f;
	}
}

void Statistics::StartTimer()
{
	m_startTick = cv::getTickCount();
}


double Statistics::StopTimer()
{
	return (double)(cv::getTickCount() - m_startTick) / cv::getTickFrequency() * 1000;
}

void Statistics::Add(TIME_PERIOD id, double count)
{
	map<TIME_PERIOD, TimerElement>::iterator i = m_elementList.find(id);
	if (i == m_elementList.end())
	{
		m_elementList[id] = TimerElement();
		i = m_elementList.find(id);
	}
	TimerElement &te = i->second;
	te.totalTime += count;
	te.frameCount++;

	if (te.frameCount % RESET_FRAME_COUNT == 0)
	{
		te.aveTimeList.push_back(te.totalTime / RESET_FRAME_COUNT);
		te.frameCount = 0;
		te.totalTime = 0.0f;
	}
}

void Statistics::Print(TIME_PERIOD id, bool printWhenReset)
{
	map<TIME_PERIOD, TimerElement>::iterator i = m_elementList.find(id);
	if (i == m_elementList.end())
	{
		return;
	}
	TimerElement &te = i->second;
	if (te.aveTimeList.empty())
	{
		return;
	}

	if (printWhenReset)
	{
		if (te.frameCount % RESET_FRAME_COUNT != 0)
		{
			return;
		}
	}

	if (id == PERIOD_TOTAL)
		Log("Statistics #########################################");

	Log("Statistics %s : %lf", timerPeriodName[id], te.aveTimeList.back());
}

void Statistics::ExportToFile(string fileName)
{
	ofstream fout(fileName.c_str(), std::ofstream::out);
	if (!fout.is_open())
		return;
	for (int i = 0; i < PERIOD_NUM; i++)
	{
		map<TIME_PERIOD, TimerElement>::iterator iter = m_elementList.find((TIME_PERIOD)i);
		if (iter == m_elementList.end())
		{
			continue;
		}

		fout << timerPeriodName[i] << " : ";
		TimerElement &te = iter->second;
		for (size_t j = 0; j < te.aveTimeList.size(); j++)
		{
			fout << te.aveTimeList[j] << ", ";
		}
		fout << endl;
		//fout << "#########################################" << endl;
	}
	fout.close();
}

void Statistics::Reset()
{
	m_elementList.clear();
}

double Statistics::GetAveTime(TIME_PERIOD id)
{
	map<TIME_PERIOD, TimerElement>::iterator iter = m_elementList.find((TIME_PERIOD)id);
	if (iter == m_elementList.end())
	{
		return -1.0;
	}

	TimerElement &te = iter->second;
	if (te.frameCount <= 0)
	{
		return -1.0;
	}
	return te.totalTime / te.frameCount;
}

double Statistics::GetCurTime(TIME_PERIOD id)
{
	map<TIME_PERIOD, TimerElement>::iterator i = m_elementList.find(id);
	if (i == m_elementList.end())
	{
		return 0.0;
	}
	TimerElement &te = i->second;
	if (te.aveTimeList.empty())
	{
		return 0.0;
	}

	return te.aveTimeList.back();
}

void Statistics::fps(FPS_TYPE id)
{
	map<FPS_TYPE, FpsElement>::iterator i = m_fpsList.find(id);
	if (i == m_fpsList.end())
	{
		m_fpsList[id] = FpsElement();
		i = m_fpsList.find(id);
	}

	i->second.stampList.push_back(cv::getTickCount());
	if (i->second.stampList.size() > FPS_FRAME_COUNT)
		i->second.stampList.pop_front();
}

double Statistics::getFps(FPS_TYPE id)
{
	map<FPS_TYPE, FpsElement>::iterator i = m_fpsList.find(id);
	if (i == m_fpsList.end())
	{
		return -1.0f;
	}

	if (i->second.stampList.size() < FPS_FRAME_COUNT)
	{
		return 0.0f;
	}

	int intervalCount = (int)(i->second.stampList.size()) - 1;
	double totalTime = (double)(i->second.stampList.back() - i->second.stampList.front()) / cv::getTickFrequency();

	return (double)intervalCount / (double)totalTime;
}