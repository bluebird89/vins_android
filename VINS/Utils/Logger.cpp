﻿/*==============================================================================
Copyright 2017 Maxst, Inc. All Rights Reserved.
==============================================================================*/


#include "Utils/Logger.h"
#include <stdio.h>
#include <stdarg.h>

#include <iostream>
#include <fstream>
using namespace std;

#define MAX_MESSAGE 10000

#ifdef WIN32
#include <windows.h>
#endif


#if !defined(__MacOS__)
void Log(const char* format, ...)
{
	char szTempBuf[MAX_MESSAGE];
	va_list marker;

	va_start(marker, format);
    vsprintf_s(szTempBuf, format, marker);

	va_end(marker);

	sprintf_s(szTempBuf, MAX_MESSAGE, "%s\n", szTempBuf);
	printf_s(szTempBuf);

	OutputDebugStringA(szTempBuf);
}
#else
void Log(const char* format, ...)
{
    char szTempBuf[MAX_MESSAGE];
    va_list marker;
    
    va_start(marker, format);
    vsprintf(szTempBuf, format, marker);
    
    va_end(marker);
    
    printf("%s\n",szTempBuf);
    ofstream myfile;
    myfile.open ("./log.txt", std::ofstream::app);
    myfile << szTempBuf << endl;
    myfile.close();
}
#endif

#if !defined(__MacOS__)
void LogD(const char* format, ...)
{
#ifdef _DEBUG
	char szTempBuf[MAX_MESSAGE];
	va_list marker;

	va_start(marker, format);
	vsprintf_s(szTempBuf, format, marker);

	va_end(marker);

	sprintf_s(szTempBuf, MAX_MESSAGE, "%s\n", szTempBuf);
	printf_s(szTempBuf);

	OutputDebugStringA(szTempBuf);
#endif
}
#else
void LogD(const char* format, ...)
{
    char szTempBuf[MAX_MESSAGE];
    va_list marker;
    
    va_start(marker, format);
    vsprintf(szTempBuf, format, marker);
    
    va_end(marker);
    
    printf("%s\n",szTempBuf);
    ofstream myfile;
    myfile.open ("./logD.txt", std::ofstream::app);
    myfile << szTempBuf << endl;
    myfile.close();
}
#endif
