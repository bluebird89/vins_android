//
//  initial_aligment.hpp
//  VINS_ios
//
//  Created by HKUST Aerial Robotics on 2016/12/26.
//  Copyright © 2017 HKUST Aerial Robotics. All rights reserved.
//

#ifndef initial_aligment_hpp
#define initial_aligment_hpp

#include <stdio.h>
#include <Eigen/Dense>
#include <iostream>
#include "factor/imu_factor.h"
#include "MatrixUtil.hpp"
#include <map>
#include "FeatureManager/feature_manager.hpp"

using namespace Eigen;
using namespace std;

class ImageFrame
{
public:
    ImageFrame(){};
    ImageFrame(const map<int, Vector3d>& _points, double _t)
    {
		points = _points;
		t = _t;
		is_key_frame = false;
    };

	void drawResult(cv::Mat result) {
#if WIN32
		map<int, Vector3d>::iterator iter = points.begin();
		Matrix4d poseMatrix;
		poseMatrix.setZero();
		poseMatrix.block<3, 3>(0, 0) = R;
		poseMatrix.block<3, 1>(0, 3) = T;
		poseMatrix(3, 3) = 1.0;
		//cout << poseMatrix << endl;

		for (; iter != points.end(); iter++) {
			Vector4d drawPoint(iter->second.x(), iter->second.y(), iter->second.z(), 1);
			cv::putText(result, to_string(iter->first), cv::Point(drawPoint.x() * FOCUS_LENGTH_X + PX, drawPoint.y() * FOCUS_LENGTH_Y + PY), CV_FONT_HERSHEY_COMPLEX, 0.5, cv::Scalar(255), 1);
			cv::circle(result, cv::Point(drawPoint.x() * FOCUS_LENGTH_X + PX, drawPoint.y() * FOCUS_LENGTH_Y + PY), 5, cv::Scalar(255));
		}
		cv::imshow("DrawResult", result);
		cv::waitKey(1);
#endif
	}

    map<int, Vector3d> points;
    double t;
    Matrix3d R;
    Vector3d T;
    IntegrationBase *pre_integration;
    bool is_key_frame;
};

class VisualIMUFunction{
public:
    static bool VisualIMUAlignment(map<double, ImageFrame> &all_image_frame, Vector3d* Bgs, Vector3d &g, VectorXd &x);

private:
    static void solveGyroscopeBias(map<double, ImageFrame> &all_image_frame, Vector3d* Bgs);
    static MatrixXd TangentBasis(Vector3d &g0);
    static void RefineGravity(map<double, ImageFrame> &all_image_frame, Vector3d &g, VectorXd &x);
    static bool SolveScale(map<double, ImageFrame> &all_image_frame, Vector3d &g, VectorXd &x);
};

#endif /* initial_aligment_hpp */
