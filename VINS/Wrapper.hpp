//
// Created by blueb on 2018-02-26.
//
#pragma once
#ifdef __ANDROID__
#include "jni.h"
#endif 

#include <vector>
#include <queue>
#include <cstdint>
#include <math.h>
#include <memory>
#include <mutex>
#include <thread>
#include <condition_variable>

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#include <opencv2/opencv.hpp>
#include <Eigen/Eigen>

#include "feature/feature_tracker.hpp"
#include "loop/keyframe_database.h"
#include "loop/loop_closure.h"

#include "Renderer/QuadRenderer.h"

#include "VINS.hpp"

#include "Utils/Logger.h"

#ifdef WIN32
#include <Windows.h>
#endif


using namespace std;
using namespace Eigen;

struct IMU_MSG
{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	double header;
	Vector3d acc;
	Vector3d gyr;
};

struct IMG_MSG
{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	double header;
	map<int, Vector3d> point_clouds;
};

struct IMG_DATA
{
	double header;
	cv::Mat image;
};

struct IMG_DATA_CACHE
{
	double header;
	cv::Mat image;
};

struct VINS_DATA_CACHE
{
	double header;
	Vector3f P;
	Matrix3f R;
};

typedef shared_ptr <IMU_MSG const > ImuConstPtr;
typedef shared_ptr <IMG_MSG const > ImgConstPtr;

static void sleep_thread(float millis) {
#ifdef WIN32
	Sleep(millis);
#else
	usleep(millis * 1000);
#endif
}

class Wrapper
{
public:
	Wrapper() {
		initialize();
	}

	VINS vins;

	FeatureTracker featureTracker;

	KeyFrameDatabase keyframe_database;

	LoopClosure* loop_closure;

	mutex m_buf;
	mutex m_imu_feedback;
	mutex m_depth_feedback;
	mutex m_image_buf_loop;

	thread mainLoop;
	thread loopThread;
	thread globalLoopThread;

	condition_variable con;

	IMG_DATA imgData;
	IMU_MSG imuData;

	IMG_DATA_CACHE image_data_cache;

	queue<IMG_DATA_CACHE> image_pool;
	queue<VINS_DATA_CACHE> vins_pool;

	queue<ImgConstPtr> img_msg_buf;
	queue<ImuConstPtr> imu_msg_buf;
	queue<IMU_MSG_LOCAL> local_imu_msg_buf;
	queue<IMG_DATA> imgDataBuf;

	queue<pair<cv::Mat, double>> image_buf_loop;

	list<IMG_MSG_LOCAL> solved_features;

	VINS_RESULT solved_vins;

	// Set true :  30Hz pose output and AR rendering in front-end(low latency)
	// Set false : 10Hz pose output and ARrendering in back-end
	bool USE_PNP = false;
	bool cameraMode = true;
	bool imageCacheEnabled = cameraMode && !USE_PNP;

	bool box_in_trajectory = true;

	bool ui_main = false;
	bool start_show = false;

	int keyframe_freq = 0;

	int frame_cnt = 0;
	int global_frame_cnt = 0;
	int loop_check_cnt = 0;

	int old_index = -1;
	int loop_old_index = -1;

	int wating_lists = 0;

	int segmentation_index = 0;

	bool isCapturing = false;
	double lateast_imu_time = -1;

	cv::Mat lateast_equa;
	cv::Mat lateast_image;
	Vector3f lateast_P;
	Matrix3f lateast_R;


	Vector3d pnp_P;
	Matrix3d pnp_R;
    
    float VERTEX_BUF[12] = {
                  -0.5f, -0.5f, 2.0f, // top left
                  -0.5f, 0.5f, 2.0f, // bottom left
                  0.5f, 0.5f, 2.0f, // bottom right
                  0.5f, -0.5f, 2.0f  // top right
    };
    float INDEX_BUF[6] = {0, 1, 2, 2, 3, 0};
    float TEXTURE_COORD_BUF[8] = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f};
    
    string vertexShader =
        "attribute vec3 vertexModel;\n"
        "uniform mat4 uMVPMatrix;\n"
        "attribute vec2 vertexUV;\n"
        "varying vec2 UV;\n"
        "void main() {\n"
            "gl_Position = uMVPMatrix * vec4(vertexModel, 1.0);\n"
            "UV = vertexUV;\n"
        "}\n";
    
	string fragShader =
		"precision mediump float;\n"
		"uniform sampler2D mytexture;\n"
		"varying vec2 UV;\n"
		"void main() {\n"
		"gl_FragColor = texture2D(mytexture, UV);\n"
		"}\n";

	QuadRenderer quadRenderer;
	bool rendererInit = false;
    
    bool drawQuad(){
		/*if (rendererInit == false) {
			quadRenderer.init(vertexShader, fragShader, false);
			rendererInit = true;
		}
		//Log("imageDataBuf size : %d", imgDataBuf.size());
		if (lateast_image.empty() == false) {
			quadRenderer.display(lateast_image.cols, lateast_image.rows, lateast_image.data, 1);
		}
		*/
        return true;
    }

	void initialize() {
		Log("initialize");
		vins.setIMUModel();

		//_condition = [[NSCondition alloc] init];
		//mainLoop = [[NSThread alloc]initWithTarget:self selector : @selector(run) object:nil];
		//[mainLoop setName : @"mainLoop"];
		mainLoop = thread(run, this);

		if (LOOP_CLOSURE == true) {
			loopThread = thread(loop_thread, this);
			globalLoopThread = thread();
		}

		vins.setExtrinsic();
		vins.setIMUModel();
		featureTracker.vins_pnp.setExtrinsic();
		featureTracker.vins_pnp.setIMUModel();

		setGlobalParam(DeviceType::iPhone6sP);

        isCapturing = true;
		Log("initialize end");
	}

	static void run(void* param) {
		Wrapper* wrapper = (Wrapper*)param;
		//[_condition lock];
		while (true) {
			wrapper->process();
			sleep_thread(0.01);
		} 
		//[_condition unlock];
	}

	int kf_global_index = 0;
	bool start_global_optimization = false;

	void process() {
		Log("Process");
		vector<pair<vector<ImuConstPtr>, ImgConstPtr>> measurements;
		unique_lock<mutex> lk(m_buf);
		Log("Process con start");
		//con.wait_for(lk, chrono::milliseconds(10), [&] {return (measurements = getMeasurements()).size() != 0; });
		con.wait(lk, [&] {return (measurements = getMeasurements()).size() != 0; });
		Log("Process con wait");
		lk.unlock();

		wating_lists = measurements.size();
		Log("imu process wating list size : %d", wating_lists);

        //if(wating_lists == 0){
       //     return;
       // }

		for (auto& measurement : measurements) {
			Log("measurement first size : %d", measurement.first.size());
			for (int j = 0; j < measurement.first.size(); j++) {
				send_imu(measurement.first[j]);
			}

			ImgConstPtr img_msg = measurement.second;
			map<int, Vector3d> image = img_msg->point_clouds;

			double header = img_msg->header;

			vins.processImage(image, header, wating_lists);
			
			if (vins.solver_flag == vins.NON_LINEAR) {
				m_depth_feedback.lock();
				solved_vins.header = vins.Headers[WINDOW_SIZE - 1];
				solved_vins.Ba = vins.Bas[WINDOW_SIZE - 1];
				solved_vins.Bg = vins.Bgs[WINDOW_SIZE - 1];
				solved_vins.P = vins.correct_Ps[WINDOW_SIZE - 1].cast<double>();
				solved_vins.R = vins.correct_Rs[WINDOW_SIZE - 1].cast<double>();
				solved_vins.V = vins.Vs[WINDOW_SIZE - 1];
				
				Vector3d R_ypr = MatrixUtil::R2ypr(solved_vins.R);
				solved_features.clear();

				for (auto& it_per_id : vins.f_manager.feature) {
					it_per_id.used_num = it_per_id.feature_per_frame.size();

					if (!(it_per_id.used_num >= 2 && it_per_id.start_frame < WINDOW_SIZE - 2)) {
						continue;
					}

					if (it_per_id.solve_flag != 1) {
						continue;
					}

					int imu_i = it_per_id.start_frame;
					Vector3d pts_i = it_per_id.feature_per_frame[0].point * it_per_id.estimated_depth;
					IMG_MSG_LOCAL tmp_feature;
					tmp_feature.id = it_per_id.feature_id;
					tmp_feature.position = vins.r_drift * vins.Rs[imu_i] * (vins.ric * pts_i + vins.tic) + vins.r_drift * vins.Ps[imu_i] + vins.t_drift;
					tmp_feature.track_num = (int)it_per_id.feature_per_frame.size();
					solved_features.push_back(tmp_feature);
				}
				m_depth_feedback.unlock();
			}

			if (imageCacheEnabled == true) {
				if (vins.solver_flag == VINS::NON_LINEAR && start_show == true) {
					VINS_DATA_CACHE vins_data_cache;
					vins_data_cache.header = vins.Headers[WINDOW_SIZE - 1];
					vins_data_cache.P = vins.correct_Ps[WINDOW_SIZE - 1];
					vins_data_cache.R = vins.correct_Rs[WINDOW_SIZE - 1];
					vins_pool.push(vins_data_cache);
				}
				else if(vins.failure_occur == true) {
					vins.drawresult.change_color = true;
					vins.drawresult.indexs.push_back(vins.drawresult.pose.size());
					segmentation_index++;
					keyframe_database.max_seg_index++;
					keyframe_database.cur_seg_index = keyframe_database.max_seg_index;

					while (!vins_pool.empty()) {
						vins_pool.pop();
					}
				}
			}

			if (LOOP_CLOSURE == true) {
				static bool first_frame = true;

				if (vins.solver_flag != vins.NON_LINEAR) {
					first_frame = true;
				}

				if (vins.marginalization_flag == vins.MARGIN_OLD && vins.solver_flag == vins.NON_LINEAR && !image_buf_loop.empty()) {
					first_frame = false;

					if (keyframe_freq % LOOP_FREQ == 0) {
						keyframe_freq = 0;

						Vector3d T_w_i = vins.Ps[WINDOW_SIZE - 2];
						Matrix3d R_w_i = vins.Rs[WINDOW_SIZE - 2];

						m_image_buf_loop.lock();
						while (!image_buf_loop.empty() && image_buf_loop.front().second < vins.Headers[WINDOW_SIZE - 2]) {
							image_buf_loop.pop();
						}

						if (vins.Headers[WINDOW_SIZE - 2] == image_buf_loop.front().second) {
							const char* pattern_file = "Resources/brief_pattern.yml";
							
							KeyFrame* keyframe = new KeyFrame(vins.Headers[WINDOW_SIZE - 2], global_frame_cnt, T_w_i, R_w_i, image_buf_loop.front().first,
								pattern_file, keyframe_database.cur_seg_index);

							keyframe->setExtrinsic(vins.tic, vins.ric);
							keyframe->buildKeyFrameFeatures(vins);

							global_frame_cnt++;
						}
						m_image_buf_loop.unlock();
					}
					else {
						first_frame = false;
					}

					for (int i = 0; i < WINDOW_SIZE; i++) {

						if (vins.Headers[i] = vins.front_pose.header) {

							KeyFrame* cur_kf = keyframe_database.getKeyframe(vins.front_pose.cur_index);

							if (fabs(vins.front_pose.relative_yaw) > 30.0 || vins.front_pose.relative_t.norm() > 10.0) {
								Log("Wrong loop\n");
								break;
							}
							cur_kf->updateLoopConnection(vins.front_pose.relative_t, vins.front_pose.relative_q, vins.front_pose.relative_yaw);
							break;
						}
					}

					int search_cnt = 0;
					for (int i = 0; i < keyframe_database.size(); i++) {
						search_cnt++;
						KeyFrame* kf = keyframe_database.getLastKeyframe(i);

						if (kf->header == vins.Headers[0]) {
							kf->updateOriginPose(vins.Ps[0], vins.Rs[0]);

							if (kf->has_loop) {
								kf_global_index = kf->global_index;
								start_global_optimization = true;
							}
							break;
						}
						else {
							if (search_cnt > 2 * WINDOW_SIZE) {
								break;
							}
						}
					}

					keyframe_freq++;

				}
			}

			wating_lists--;

			//finish solve one frame
			//[self performSelectorOnMainThread : @selector(showInputView) withObject:nil waitUntilDone : YES];
		}
		Log("process end");
	}

	void processImage(unsigned char* buffer, int width, int height, long long int timestamp) {
		Log("processImage start");
		cv::Mat image(height, width, CV_8UC1, buffer);

		if (isCapturing == true) {
			shared_ptr<IMG_MSG> img_msg = make_shared<IMG_MSG>();
			img_msg->header = timestamp;
			if (lateast_imu_time <= 0) {
				Log("processImage lateast return");
				return;
			}

			//img_msg->header = timestamp;
			//bool isNeedRotation = image.size() != frameSize;

			cv::Mat input_frame;
//			if (start_playback) {		//이 하위는 실질적으로 안탐
//				//TS(readImg);
//				bool still_play;
//				still_play = [self readImageTime : imageDataReadIndex];
//				[self readImage : imageDataReadIndex];
//				if (!still_play)
//					return;
//				imageDataReadIndex++;
//#ifdef DATA_EXPORT
//				[self tapSaveImageToIphone : imgData.image];
//#endif
//				UIImageToMat(imgData.image, image);
//				UIImageToMat(imgData.image, input_frame);
//				img_msg->header = imgData.header;
//				//TE(readImg);
//#ifdef DATA_EXPORT
//				printf("record play image: %lf\n", imgData.header, imageDataReadIndex);
//#endif
//			}
//			else
			{
				input_frame = image;
			}

			if (start_record) {
				imgData.header = img_msg->header;
				imgData.image = image.clone();
				imgDataBuf.push(imgData);
				Log("processImage start_record return");
				return;
			}
			else {
				if (!imgDataBuf.empty()) {
					Log("processImage imgDataBuf return");
					return;
				}
			}

			//prevTime = mach_absolute_time();
			cv::Mat gray = image.clone();
			cv::Mat img_with_feature;
			cv::Mat img_equa;
			cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
			clahe->setClipLimit(3);
			clahe->apply(gray, img_equa);

			m_depth_feedback.lock();
			featureTracker.solved_features = solved_features;
			featureTracker.solved_vins = solved_vins;
			m_depth_feedback.unlock();

			m_imu_feedback.lock();

			vector<Point2f> good_pts;
			vector<double> track_len;
			bool vins_normal = (vins.solver_flag == VINS::NON_LINEAR);
			featureTracker.use_pnp = USE_PNP;

			featureTracker.readImage(img_equa, img_with_feature, frame_cnt, good_pts, track_len, img_msg->header, pnp_P, pnp_R, vins_normal);

			for (int i = 0; i < good_pts.size(); i++) {
				circle(image, good_pts[i], 0, cv::Scalar(255 * (1 - track_len[i])), 7);
			}

			if (featureTracker.img_cnt == 0) {
				img_msg->point_clouds = featureTracker.image_msg;

				m_buf.lock();
				img_msg_buf.push(img_msg);
				m_buf.unlock();

				Log("notify one start");
				con.notify_one();
				Log("notify one end");

				if (imageCacheEnabled == true) {
					image_data_cache.header = img_msg->header;
					image_data_cache.image = gray.clone();
					image_pool.push(image_data_cache);
				}

				if (LOOP_CLOSURE == true) {
					m_image_buf_loop.lock();
					cv::Mat loop_image = gray.clone();
					image_buf_loop.push(make_pair(loop_image, img_msg->header));

					if (image_buf_loop.size() > WINDOW_SIZE) {
						image_buf_loop.pop();
					}

					m_image_buf_loop.unlock();
				}
			}

			featureTracker.img_cnt = (featureTracker.img_cnt + 1) % FREQ;

			for (int i = 0; i < good_pts.size(); i++) {
				circle(image, good_pts[i], 0, cv::Scalar(255 * (1 - track_len[i])), 7);
			}

			if (imageCacheEnabled) {
				if (!vins_pool.empty() && !image_pool.empty()) {
					while (vins_pool.size() > 1) {
						vins_pool.pop();
					}

					while (!image_pool.empty() && image_pool.front().header < vins_pool.front().header) {
						image_pool.pop();
					}

					if (!vins_pool.empty() && !image_pool.empty()) {
						lateast_image = image_pool.front().image.clone();
						lateast_P = vins_pool.front().P;
						lateast_R = vins_pool.front().R;
						gray = lateast_image;
					}
				}
				else if (!image_pool.empty()) {
					if (image_pool.size() > 10) {
						image_pool.pop();
					}
				}
			}

			if (USE_PNP == true) {
				lateast_P = pnp_P.cast<float>();
				lateast_R = pnp_R.cast<float>();
			}

			if (ui_main == true || start_show == false || vins.solver_flag != VINS::NON_LINEAR) {

				if (vins.solver_flag == VINS::NON_LINEAR && start_show == true) {
					cv::Mat tmp;

					vins.drawresult.startInit = true;
					vins.drawresult.drawAR(vins.imageAI, vins.correct_point_cloud, lateast_P, lateast_R);

					tmp = gray.clone();
					cv::Mat mask;
					cv::Mat imageAI = vins.imageAI;

					//todo : 이 부분 다시 살펴볼것.  -- processImage -a
					if (!imageAI.empty()) {
						mask = imageAI.clone();
					}

					imageAI.copyTo(tmp, mask);
					/*image = tmp.clone();
					cv::Mat tmp;
					vins.drawresult.startInit = true;
					vins.drawresult.drawAR(vins.imageAI, vins.correct_point_cloud, lateast_P, lateast_R);

					cv::cvtColor(image, tmp, CV_RGBA2BGR);
					cv::Mat mask;
					cv::Mat imageAI = vins.imageAI;
					if (!imageAI.empty())
						cv::cvtColor(imageAI, mask, CV_RGB2GRAY);
					imageAI.copyTo(tmp, mask);
					cv::cvtColor(tmp, image, CV_BGRA2BGR);*/

					//todo end -- processImage -a

				}

				if (DEBUG_MODE == true) {
					cv::flip(lateast_equa, image, -1);
				}
				else {
					cv::flip(image, image, -1);
					if (vins.solver_flag != VINS::NON_LINEAR || start_show == false) {
						//필요없는 코드...
						//cv::cvtColor(image, image, CV_RGBA2BGR;
					}
				}
			}
			else {
				if (vins.solver_flag == VINS::NON_LINEAR) {
					vins.drawresult.pose.clear();
					vins.drawresult.pose = keyframe_database.refine_path;
					vins.drawresult.segment_indexs = keyframe_database.segment_indexs;
					vins.drawresult.Reprojection(vins.image_show, vins.correct_point_cloud, vins.correct_Rs, vins.correct_Ps, box_in_trajectory);
				}

				//필요한지....?
				/*cv::Mat tmp2 = vins.image_show;

				cv::Mat down_origin_image;
				cv::resize(image.t(), down_origin_image, cv::Size(200, 150));
				cv::cvtColor(down_origin_image, down_origin_image, CV_BGRA2RGB);
				cv::flip(down_origin_image, down_origin_image, 0);
				cv::Mat imageROI;
				imageROI = tmp2(cv::Rect(10, COL - down_origin_image.rows - 10, down_origin_image.cols, down_origin_image.rows));
				cv::Mat mask;
				cv::cvtColor(down_origin_image, mask, CV_RGB2GRAY);
				down_origin_image.copyTo(imageROI, mask);


				cv::cvtColor(tmp2, image, CV_BGRA2BGR);
				cv::flip(image, tmp2, 1);
				if (isNeedRotation)
					image = tmp2.t();*/
			}
		}
		else {
			cv::flip(image, image, -1);
		}
		Log("processImage end");
	}

	bool start_playback = false;
	bool start_record = false;
	bool imuDataFinished = false;
	int imu_prepare = 0;
	unsigned int imuDataIndex = 0;
	//shared_ptr<IMU_MSG> cur_acc = make_shared<IMU_MSG>();
    queue<shared_ptr<IMU_MSG>> cur_acc_queue;
	vector<IMU_MSG> gyro_buf;

	void imuStartUpdate(float* data, long long int timestamp, int type) {
		if (type == 0) {                      //acc
            Log("acc input");
			if (imu_prepare < 10) {
				imu_prepare++;
			}

			shared_ptr<IMU_MSG> acc_msg = make_shared<IMU_MSG>();
			acc_msg->header = (double)timestamp;
			acc_msg->acc << -data[0], -data[1], -data[2];   //iphone 은 단위가 g이고 안드로이드는 m/s^2임.
			cur_acc_queue.push(acc_msg);
		}
		else if (type == 1) {               //gyro
            Log("gyro input");
			if (imu_prepare < 10) {
				return;
			}

			IMU_MSG gyro_msg;
			gyro_msg.header = (double)timestamp;
			gyro_msg.gyr << data[0], data[1], data[2];

			if (gyro_buf.size() == 0) {
				gyro_buf.push_back(gyro_msg);
				gyro_buf.push_back(gyro_msg);
				return;
			}
			else {
				gyro_buf[0] = gyro_buf[1];
				gyro_buf[1] = gyro_msg;
			}

			while (!cur_acc_queue.empty() && cur_acc_queue.front()->header < gyro_buf[0].header) {
				cur_acc_queue.pop();
			}

			vector<shared_ptr<IMU_MSG>> imu_msg_vector;

			while (!cur_acc_queue.empty() &&cur_acc_queue.front()->header <= gyro_buf[1].header) {
				shared_ptr<IMU_MSG> cur_acc = cur_acc_queue.front();
				shared_ptr<IMU_MSG> imu_msg = make_shared<IMU_MSG>();
				imu_msg->header = cur_acc->header;
				imu_msg->acc = cur_acc->acc;

				if (cur_acc->header == gyro_buf[1].header) {
					imu_msg->gyr = gyro_buf[1].gyr;
				}
				else if (cur_acc->header == gyro_buf[0].header) {
					imu_msg->gyr = gyro_buf[0].gyr;
				}
				else {
					imu_msg->gyr = gyro_buf[0].gyr + (cur_acc->header - gyro_buf[0].header) * (gyro_buf[1].gyr - gyro_buf[0].gyr) / (gyro_buf[1].header - gyro_buf[0].header);
				}

				cur_acc_queue.pop();

				imu_msg_vector.push_back(imu_msg);
			}
//#ifdef READ_VINS		//이 하위는 실질적으로 안탐.
//			if (start_playback_vins) {
//				if (vinsDataFinished)
//					return;
//				NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//				NSString *documentsPath = [paths objectAtIndex : 0];
//				NSString *filePath = [documentsPath stringByAppendingPathComponent : @"VINS"];
//				vinsReader = [NSData dataWithContentsOfFile : filePath];
//
//				[vinsReader getBytes : &vinsData range : NSMakeRange(vinsDataReadIndex * sizeof(vinsData), sizeof(vinsData))];
//				vinsDataReadIndex++;
//				if (vinsData.header == 0) {
//					printf("record play vins finished\n");
//					vinsDataFinished = true;
//					return;
//				}
//				printf("record play vins: %4d, %lf %lf %lf %lf %lf %lf %lf %lf\n", vinsDataReadIndex, vinsData.header, vinsData.translation.x(), vinsData.translation.y(), vinsData.translation.z(),
//					vinsData.rotation.w(), vinsData.rotation.x(), vinsData.rotation.y(), vinsData.rotation.z());
//			}
//#endif
//			if (start_playback) {
//				//TS(read_imu_buf);
//				if (imuDataFinished)
//					return;
//				[imuReader getBytes : &imuData range : NSMakeRange(imuDataReadIndex * sizeof(imuData), sizeof(imuData))];
//				imuDataReadIndex++;
//				if (imuData.header == 0) {
//					imuDataFinished = true;
//					return;
//				}
//				imu_msg->header = imuData.header;
//				imu_msg->acc = imuData.acc;
//				imu_msg->gyr = imuData.gyr;
//				//TE(read_imu_buf);
//#ifdef DATA_EXPORT
//				printf("record play imu: %lf %lf %lf %lf %lf %lf %lf\n", imuData.header, imu_msg->acc.x(), imu_msg->acc.y(), imu_msg->acc.z(),
//					imu_msg->gyr.x(), imu_msg->gyr.y(), imu_msg->gyr.z());
//#endif
//			}
			int imu_msg_size = imu_msg_vector.size();
			
			//Log("imu msg size : %d", imu_msg_size);

			if (imu_msg_size == 0) {
				Log("imu sensor error");
				return;
			}

			if (start_record) {
				for (int i = 0; i < imu_msg_size; i++) {
					imuData.header = imu_msg_vector[i]->header;
					imuData.acc = imu_msg_vector[i]->acc;
					imuData.gyr = imu_msg_vector[i]->gyr;

					//[imuDataBuf appendBytes : &imuData length : sizeof(imuData)];
					imuDataIndex++;
					//NSLog(@"record: imu %lf, %lu",imuData.header,imuDataIndex);
				}
			}

			lateast_imu_time = imu_msg_vector[imu_msg_vector.size() - 1]->header;
			//img_msg callback
			m_imu_feedback.lock();
			for (int i = 0; i < imu_msg_size; i++) {
				IMU_MSG_LOCAL imu_msg_local;
				imu_msg_local.header = imu_msg_vector[i]->header;
				imu_msg_local.acc = imu_msg_vector[i]->acc;
				imu_msg_local.gyr = imu_msg_vector[i]->gyr;

				local_imu_msg_buf.push(imu_msg_local);
			}
			m_imu_feedback.unlock();
			m_buf.lock();
			for (int i = 0; i < imu_msg_size; i++) {
				imu_msg_buf.push(imu_msg_vector[i]);
				//NSLog(@"IMU_buf timestamp %lf, acc_x = %lf",imu_msg_buf.front()->header,imu_msg_buf.front()->acc.x());
			}
			m_buf.unlock();
			con.notify_one();
			Log("gyro end");
		}
	}

	bool voc_init_ok = false;

	static void loop_thread(void* param) {
		Wrapper* wrapper = (Wrapper*)param;

		if (LOOP_CLOSURE == true && wrapper->loop_closure == nullptr) {
			Log("Loop start, load voc");
			const char* voc_file = "Resources/brief_k10L6.bin";
			wrapper->loop_closure = new LoopClosure(voc_file, COL, ROW);
			Log("load voc finish");

			wrapper->voc_init_ok = true;
		}

		while (true) {	//여기 확인 필요함.
			Log("loop thread");
			if (!LOOP_CLOSURE) {
				sleep_thread(0.5);
				continue;
			}
			/*while (![[NSThread currentThread] isCancelled]) {	//여기 확인 필요함.
				if (!LOOP_CLOSURE) {
					[NSThread sleepForTimeInterval : 0.5];
					continue;
				}*/

			bool loop_succ = false;

			if (wrapper->loop_check_cnt < wrapper->global_frame_cnt) {
				KeyFrame* cur_kf = wrapper->keyframe_database.getLastKeyframe();
				wrapper->loop_check_cnt++;
				cur_kf->check_loop = 1;

				cv::Mat current_image = cur_kf->image;

				vector<cv::Point2f> measurements_old;
				vector<cv::Point2f> measurements_old_norm;
				vector<cv::Point2f> measurements_cur;
				vector<int> features_id;
				vector<cv::Point2f> measurements_cur_origin = cur_kf->measurements;

				vector<cv::Point2f> cur_pts;
				vector<cv::Point2f> old_pts;
				
				cur_kf->extractBrief(current_image);
				Log("loop extract %d featrue\n", cur_kf->keypoints.size());

				loop_succ = wrapper->loop_closure->startLoopClosure(cur_kf->keypoints, cur_kf->descriptors, cur_pts, old_pts, wrapper->old_index);

				if (loop_succ == true) {
					KeyFrame* old_kf = wrapper->keyframe_database.getKeyframe(wrapper->old_index);

					if (old_kf == nullptr) {
						Log("NO such %dth frame in keyframe_database\n", wrapper->old_index);
						assert(false);
					}

					Log("loop succ with %drd image\n", wrapper->old_index);
					assert(wrapper->old_index != -1);

					Vector3d T_w_i_old;
					Matrix3d R_w_i_old;

					old_kf->getPose(T_w_i_old, R_w_i_old);
					cur_kf->findConnectionWithOldFrame(old_kf, cur_pts, old_pts, measurements_old, measurements_old_norm);

					measurements_cur = cur_kf->measurements;
					features_id = cur_kf->features_id;

					if (measurements_old_norm.size() > MIN_LOOP_NUM) {
						Quaterniond Q_loop_old(R_w_i_old);
						
						RetriveData retrive_data;
						
						retrive_data.cur_index = cur_kf->global_index;
						retrive_data.header = cur_kf->header;
						retrive_data.P_old = T_w_i_old;
						retrive_data.Q_old = Q_loop_old;
						retrive_data.use = true;
						retrive_data.measurements = measurements_old_norm;
						retrive_data.features_ids = features_id;
						wrapper->vins.retrive_pose_data = (retrive_data);

						cur_kf->detectLoop(wrapper->old_index);
						wrapper->keyframe_database.addLoop(wrapper->old_index);
						old_kf->is_looped = 1;
						wrapper->loop_old_index = wrapper->old_index;
					}
				}

				cur_kf->image.release();
			}

			if (loop_succ == true) {
				sleep_thread(2);
			}
			sleep_thread(0.05);
		}
	}

	Vector3d loop_correct_t = Vector3d(0, 0, 0);
	Matrix3d loop_correct_r = Matrix3d::Identity();

	static void global_loop_thread(void* param) {
		Wrapper* wrapper = (Wrapper*)param;

		while (true) {
			if (wrapper->start_global_optimization == true) {
				wrapper->start_global_optimization = false;

				wrapper->keyframe_database.optimize4DoFLoopPoseGraph(wrapper->kf_global_index, wrapper->loop_correct_t, wrapper->loop_correct_r);

				wrapper->vins.t_drift = wrapper->loop_correct_t;
				wrapper->vins.r_drift = wrapper->loop_correct_r;

				sleep_thread(1.17);
			}
			sleep_thread(0.03);
		}
	}

	/*
	Send imu data and visual data into VINS
	*/
	vector<pair<vector<ImuConstPtr>, ImgConstPtr>> getMeasurements() {

		vector<pair<vector<ImuConstPtr>, ImgConstPtr>> measurements;
		Log("getMeasurements");
		while (true) {
			if (imu_msg_buf.empty() || img_msg_buf.empty()) {
				Log("getMeasurements empty %d", measurements.size());
				return measurements;
			}
			Log("getMeasurements?");
			if (!(imu_msg_buf.back()->header > img_msg_buf.front()->header)) {
				Log("wait for imu, only should happen at the beginning");
				return measurements;
			}
			Log("getMeasurements11");
			if (!(imu_msg_buf.front()->header < img_msg_buf.front()->header)) {
				Log("%lf, %lf", imu_msg_buf.front()->header, img_msg_buf.front()->header);
				Log("throw img, only should happen at the beginning");
				img_msg_buf.pop();
				continue;
			}
			Log("getMeasurements22");
			ImgConstPtr img_msg = img_msg_buf.front();
			img_msg_buf.pop();

			vector<ImuConstPtr> IMUs;
			while (imu_msg_buf.front()->header <= img_msg->header) {
				IMUs.emplace_back(imu_msg_buf.front());
				imu_msg_buf.pop();
			}
			Log("getMeasurements33");
			measurements.emplace_back(IMUs, img_msg);
			Log("getMeasurements44");
		}
		return measurements;
	}

	vector<IMU_MSG_LOCAL> getImuMeasurements(double header) {

		vector<IMU_MSG_LOCAL> imu_measurements;

		static double last_header = -1;
		if (last_header < 0 || local_imu_msg_buf.empty()) {
			last_header = header;
			return imu_measurements;
		}

		while (!local_imu_msg_buf.empty() && local_imu_msg_buf.front().header <= last_header) {
			local_imu_msg_buf.pop();
		}

		while (!local_imu_msg_buf.empty() && local_imu_msg_buf.front().header <= header) {

			imu_measurements.emplace_back(local_imu_msg_buf.front());
			local_imu_msg_buf.pop();
		}

		last_header = header;

		return imu_measurements;
	}

	double current_time = -1;

	void send_imu(const ImuConstPtr &imu_msg) {
		double t = imu_msg->header;
		if (current_time < 0)
			current_time = t;
		double dt = (t - current_time);
		current_time = t;

		double ba[] {0.0, 0.0, 0.0};
		double bg[] {0.0, 0.0, 0.0};

		double dx = imu_msg->acc.x() - ba[0];
		double dy = imu_msg->acc.y() - ba[1];
		double dz = imu_msg->acc.z() - ba[2];

		double rx = imu_msg->gyr.x() - bg[0];
		double ry = imu_msg->gyr.y() - bg[1];
		double rz = imu_msg->gyr.z() - bg[2];
		//NSLog(@"IMU %f, dt: %f, acc: %f %f %f, gyr: %f %f %f", t, dt, dx, dy, dz, rx, ry, rz);

		vins.processIMU(dt, Vector3d(dx, dy, dz), Vector3d(rx, ry, rz));
	}
};
