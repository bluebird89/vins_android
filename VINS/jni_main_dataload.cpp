//
// Created by blueb on 2018-02-26.
//
#include <cstdint>
#include <string>
#include "jni.h"

#include "Utils/IMUFileReader.h"
#include "WrapperNoThreadUseMutex.hpp"
#include "Utils/Statistics.h"

extern "C"
{
    Wrapper* wrapper = new Wrapper(DeviceType::Android);
    IMUFileReader* imuFileReader = nullptr;

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_init(JNIEnv *env, jclass obj) {

		if(wrapper == nullptr) {
           wrapper = new Wrapper(DeviceType::Android);
        }

        return true;// sensorIntegration->initData();
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_setPath(JNIEnv *env, jclass obj, jstring jPath) {

        FileType fileType = FileType::AndroidFile;

        const char * tempString = env->GetStringUTFChars(jPath, 0);
        std::string path(tempString);

        imuFileReader = new IMUFileReader(path, fileType);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_getImageData(JNIEnv *env, jclass obj,
                                                             jbyteArray buffer,
                                                             jlongArray outputMillis) {

        jbyte* nativeByte = (jbyte*)env->GetPrimitiveArrayCritical(buffer, 0);
        jlong* nativeLong = (jlong*)env->GetPrimitiveArrayCritical(outputMillis, 0);

        if (nativeByte == nullptr || nativeLong == nullptr) {
            return false;
        }

        cv::Mat image;
        long long int millis = -1;

        bool read = imuFileReader->getImage(image, millis);

        nativeLong[0] = millis;
        memcpy(nativeByte, image.data, sizeof(char) * 640 * 480);

        env->ReleasePrimitiveArrayCritical(buffer, nativeByte, JNI_ABORT);
        env->ReleasePrimitiveArrayCritical(outputMillis, nativeLong, JNI_ABORT);

        return read;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_getGyroData(JNIEnv *env, jclass obj,
                                                             jfloatArray outputPose,
                                                             jlongArray outputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(outputPose, 0);
        jlong *nativeLong = (jlong *) env->GetPrimitiveArrayCritical(outputMillis, 0);

        if (nativeFloat == nullptr || nativeLong == nullptr) {
            return false;
        }

        long long int millis = -1;
        bool read = imuFileReader->getGyroData(nativeFloat, millis);
        nativeLong[0] = millis;

        env->ReleasePrimitiveArrayCritical(outputPose, nativeFloat, JNI_ABORT);
        env->ReleasePrimitiveArrayCritical(outputMillis, nativeLong, JNI_ABORT);

        return read;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_getAccData(JNIEnv *env, jclass obj,
                                                             jfloatArray outputPose,
                                                             jlongArray outputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(outputPose, 0);
        jlong *nativeLong = (jlong *) env->GetPrimitiveArrayCritical(outputMillis, 0);

        if (nativeFloat == nullptr || nativeLong == nullptr) {
            return false;
        }

        long long int millis = -1;
        bool read = imuFileReader->getAccData(nativeFloat, millis);
        nativeLong[0] = millis;

        env->ReleasePrimitiveArrayCritical(outputPose, nativeFloat, JNI_ABORT);
        env->ReleasePrimitiveArrayCritical(outputMillis, nativeLong, JNI_ABORT);

        return read;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_setGyroData(JNIEnv *env, jclass obj,
                                                                     jfloatArray inputPose,
                                                                     jlong inputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(inputPose, 0);

        if (nativeFloat == nullptr) {
            return false;
        }
        wrapper->imuStartUpdate(nativeFloat, inputMillis, 1);
        env->ReleasePrimitiveArrayCritical(inputPose, nativeFloat, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_setAccData(JNIEnv *env, jclass obj,
                                                                    jfloatArray inputPose,
                                                                    jlong inputMillis) {

        jfloat *nativeFloat = (jfloat *) env->GetPrimitiveArrayCritical(inputPose, 0);

        if (nativeFloat == nullptr) {
            return false;
        }
		wrapper->imuStartUpdate(nativeFloat, inputMillis, 0);
        env->ReleasePrimitiveArrayCritical(inputPose, nativeFloat, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_setImageData(JNIEnv *env, jclass obj,
                                                                 jbyteArray buffer, jint width, jint height,
                                                                 jlong inputMillis) {

        jbyte *nativeChar = (jbyte *) env->GetPrimitiveArrayCritical(buffer, 0);

        if (nativeChar == nullptr) {
            return false;
        }
		
        TS(time_processImage);
        wrapper->processImage((unsigned char*)nativeChar, width, height, inputMillis);
        TE(time_processImage);
        
		env->ReleasePrimitiveArrayCritical(buffer, nativeChar, JNI_ABORT);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_process(JNIEnv *env, jclass obj) {

        TS(time_process);

		wrapper->process();
        TE(time_process);

        return true;
    }

    JNIEXPORT jboolean JNICALL
    Java_com_example_vins_1dataload_JNIInterface_drawAR(JNIEnv *env, jclass obj,
															jbyteArray buffer) {

		jbyte* nativeChar = (jbyte*)env->GetPrimitiveArrayCritical(buffer, 0);

		if (nativeChar == nullptr) {
			return false;
		}

		bool result = wrapper->getDrawARImage((unsigned char*)nativeChar);

		env->ReleasePrimitiveArrayCritical(buffer, nativeChar, JNI_ABORT);

		return result;
    }

	JNIEXPORT jboolean JNICALL
		Java_com_example_vins_1dataload_JNIInterface_setBox(JNIEnv *env, jclass obj) {

		bool result = wrapper->setBox();

		return result;
	}
}
