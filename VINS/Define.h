#pragma once

#define DEBUG_MODE 1

#if DEBUG_MODE
#define IMAGE_DEBUG 0
#define LOG_DEBUG 1

#if WIN32

#define PANGOLIN_VIEWER 1
#define DRAW_RESULT 1
#else
#define PANGOLIN_VIEWER 0
#define DRAW_RESULT 0
#endif

#define OPTICAL_FLOW_VIEWER 1
#if IMAGE_DEBUG && WIN32
#define ADD_FEATURE 0
#define UPDATE_LOOP_CORRECTION 0
#define PNP_RESULT 0
#define FOWR_PTS 0
#define FRAME_DRAW_RESULT 0
#define TEMP_DRAW_AR 0
#endif


#if LOG_DEBUG
#define FUNCTION_LOG 0
#define __FUNCTION__ __func__

#define CERES_LOG 0
#define CERES_FULL_REPORT 0
#if CERES_FULL_REPORT
#define SOLVE_CERES_FULL_REPORT 1
#define CONSTRUCT_FULL_REPORT 0
#define VINS_PNP_FULL_REPORT 1
#endif

#define WRAPPER_STATE 0

#define TIMER_LOG 0

#endif
#endif